﻿namespace AdventOfCode
{
    public interface IAdventDay
    {
        int Year { get; }

        int Day { get; }

        string CalculatePartA();

        string CalculatePartB();
    }
}
