﻿using System;

namespace AdventOfCode.Attributes
{
    public class AwesomeAttribute : Attribute
    {
        // Attribute for the purpose of locating Advent Days which were really enjoyable.
    }
}
