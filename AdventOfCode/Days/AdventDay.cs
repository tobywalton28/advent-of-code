﻿namespace AdventOfCode.Days
{
    public abstract class AdventDay
    {
        private readonly InputReader _inputReader;

        public AdventDay(int year, int day)
        {
            Year = year;
            Day = day;
            _inputReader = new InputReader();
        }

        public int Year { get; }

        public int Day { get; }

        public string[] GetInput()
        {
            return _inputReader.GetInput(Year, Day);
        }
    }
}
