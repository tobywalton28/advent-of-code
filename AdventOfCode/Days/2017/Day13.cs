﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2017
{
    public class Day13 : AdventDay, IAdventDay
    {
        public Day13() : base(2017, 13)
        {
        }

        public string CalculatePartA()
        {
            var scanner = new PacketScanner(GetLayers().ToList());
            var result = scanner.CalculateSeverity(0, false);
            return result.ToString();
        }

        public string CalculatePartB()
        {
            int delay = 0;
            int severity = int.MaxValue;
            var layers = GetLayers().ToList();
            var scanner = new PacketScanner(layers);
            bool caught = true;

            while (severity > 0 && caught)
            {
                delay++;
                severity = scanner.CalculateSeverity(delay, true);
                caught = scanner.Caught;
            }

            return delay.ToString();
        }

        private IEnumerable<Layer> GetLayers()
        {
            var layers = GetInput().Select(str => new Layer(int.Parse(str.Split(':')[0].Trim()), int.Parse(str.Split(':')[1].Trim())));

            return layers;
        }

        public class PacketScanner
        {
            private Dictionary<int, Layer> _layers;
            private int _packetLocation = -1;
            private int _maxDepth;
            private int _delay = 0;
            private int _severity = 0;            
            private int _picoseconds = 0;

            public bool Caught { get; private set; }

            public PacketScanner(List<Layer> layers)
            {
                _layers = layers.ToDictionary(l => l.Depth, l => l);
                _maxDepth = layers.Max(l => l.Depth);
            }

            public void Reset()
            {
                Caught = false;
                _packetLocation = -1;

                foreach (KeyValuePair<int, Layer> layer in _layers)
                {
                    layer.Value.Reset();
                }
            }

            public int CalculateSeverity(int delay, bool exitOnCaught)
            {
                _delay = delay;
                Reset();

                _picoseconds = delay;

                while (_packetLocation < _maxDepth && (Caught == false || exitOnCaught == false))
                {
                    TraverseFirewall();
                    _picoseconds += 1;
                }

                return _severity;
            }

            public void TraverseFirewall()
            {
                _packetLocation += 1;

                var layer = _layers.ContainsKey(_packetLocation) ? _layers[_packetLocation] : null;

                if (layer != null && layer.ScannerAtTop(_picoseconds))
                {
                    _severity += layer.Severity;
                    Caught = true;
                }
            }
        }

        public class Layer
        {
            private bool _scannerMovingForwards = true;

            public Layer(int depth, int range)
            {
                Depth = depth;
                Range = range;
            }

            public void Reset()
            {
                _scannerMovingForwards = true;
            }

            public int Depth { get; private set; }

            public int Range { get; private set; }

            public int Severity => Depth * Range;

            public bool ScannerAtTop(int picoseconds)
            {
                return
                    picoseconds == 0
                    ||
                    Range == 1
                    ||
                    picoseconds % (Range * 2 - 2) == 0;
            }
        }
    }
}
