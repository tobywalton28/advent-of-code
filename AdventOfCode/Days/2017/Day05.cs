﻿using System;
using System.Linq;

namespace AdventOfCode.Days._2017
{
    public class Day05 : AdventDay, IAdventDay
    {
        private int _JumpCount = 0;
        private int _CurrentLocation = 0;
        private int[] _Maze;

        public Day05() : base(2017, 5)
        {
        }

        public string CalculatePartA()
        {
            _CurrentLocation = 0;
            _JumpCount = 0;

            _Maze = GetInput().ToList().Select(s => int.Parse(s)).ToArray();

            while (_CurrentLocation >= 0 && _CurrentLocation <= _Maze.Length - 1)
            {
                Jump((currentLocation) => currentLocation + 1);
            }


            return _JumpCount.ToString();
        }

        public string CalculatePartB()
        {
            _CurrentLocation = 0;
            _JumpCount = 0;

            _Maze = GetInput().ToList().Select(s => int.Parse(s)).ToArray();

            while (_CurrentLocation >= 0 && _CurrentLocation <= _Maze.Length - 1)
            {
                Jump((currentLocation) => currentLocation >= 3 ? currentLocation - 1 : currentLocation + 1);
            }


            return _JumpCount.ToString();
        }

        private void Jump(Func<int, int> offsetCalculator)
        {
            int jumpDistance = _Maze[_CurrentLocation];
            _Maze[_CurrentLocation] = offsetCalculator(_Maze[_CurrentLocation]);
            _CurrentLocation = _CurrentLocation + jumpDistance;

            _JumpCount++;
        }
    }
}
