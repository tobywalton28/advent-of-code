﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2017
{
    public class Day03 : AdventDay, IAdventDay
    {
        public Day03() : base(2017, 3)
        {
        }

        public string CalculatePartA()
        {
            int input = 347991;
            var loop = CalculateLoop(input);
            var coordinates = CalculateCoordinates(loop, input);

            return Math.Abs(coordinates.X) + Math.Abs(coordinates.Y).ToString();
        }

        public string CalculatePartB()
        {
            var allCells = new List<Cell> {
                new Cell(0, 0, 1)
            };

            int moves = 1;
            int currentX = 0, currentY = 0;

            while (moves < 9)
            {
                // go right moves
                for (int move = 1; move <= moves; move++)
                {
                    currentX++;
                    var cell = new Cell(currentX, currentY);
                    var adjacentCells = GetAdjacentCells(allCells, cell);
                    cell.Value = adjacentCells.Sum(c => c.Value);
                    allCells.Add(cell);
                }

                // go up moves
                for (int move = 1; move <= moves; move++)
                {
                    currentY++;
                    var cell = new Cell(currentX, currentY);
                    var adjacentCells = GetAdjacentCells(allCells, cell);
                    cell.Value = adjacentCells.Sum(c => c.Value);
                    allCells.Add(cell);
                }

                // go left moves + 1
                for (int move = 1; move <= moves + 1; move++)
                {
                    currentX--;
                    var cell = new Cell(currentX, currentY);
                    var adjacentCells = GetAdjacentCells(allCells, cell);
                    cell.Value = adjacentCells.Sum(c => c.Value);
                    allCells.Add(cell);
                }

                // go down moves + 1
                for (int move = 1; move <= moves + 1; move++)
                {
                    currentY--;
                    var cell = new Cell(currentX, currentY);
                    var adjacentCells = GetAdjacentCells(allCells, cell);
                    cell.Value = adjacentCells.Sum(c => c.Value);
                    allCells.Add(cell);
                }

                moves += 2;
            }

            int input = 347991;

            return allCells
                    .Where(c => c.Value > input)
                    .OrderBy(c => c.Value)
                    .First()
                    .Value.ToString();
        }

        private Coordinates CalculateCoordinates(Loop loop, int number)
        {
            if (loop.LoopMax == number)
                return loop.LoopMaxCoordinates;

            var currentCoordinates = new Coordinates { X = loop.LoopMaxCoordinates.X, Y = loop.LoopMaxCoordinates.Y };
            int currentNumber = loop.LoopMax;

            int moves = 0;

            //go left
            while (currentNumber != number && moves < SideLength(loop.LoopNumber) - 1)
            {
                currentNumber -= 1;
                currentCoordinates.X -= 1;
                moves++;
            }

            //go up
            moves = 0;
            while (currentNumber != number && moves < SideLength(loop.LoopNumber) - 1)
            {
                currentNumber -= 1;
                currentCoordinates.Y += 1;
                moves++;
            }

            //go right
            moves = 0;
            while (currentNumber != number && moves < SideLength(loop.LoopNumber) - 1)
            {
                currentNumber -= 1;
                currentCoordinates.X += 1;
                moves++;
            }

            //go down
            moves = 0;
            while (currentNumber != number && moves < SideLength(loop.LoopNumber) - 2)
            {
                currentNumber -= 1;
                currentCoordinates.Y -= 1;
                moves++;
            }

            if (currentNumber != number) throw new Exception("could not find number");

            return currentCoordinates;
        }

        private int SideLength(int loopNumber)
        {
            return 2 * loopNumber - 1;
        }

        private Loop CalculateLoop(int number)
        {
            int loopNumber = 1;
            int runningTotal = 1;

            while (runningTotal < number)
            {
                loopNumber++;
                int loopValues = 8 * (loopNumber - 1);
                runningTotal += loopValues;
            }

            return new Loop
            {
                LoopNumber = loopNumber,
                LoopMax = runningTotal,
                LoopMaxCoordinates = new Coordinates { X = loopNumber - 1, Y = -(loopNumber - 1) }
            };
        }

        private class Loop
        {
            public int LoopNumber { get; set; }

            public int LoopMax { get; set; }

            public Coordinates LoopMaxCoordinates { get; set; }
        }

        private class Coordinates
        {
            public int X { get; set; }

            public int Y { get; set; }
        }

        IEnumerable<Cell> GetAdjacentCells(IEnumerable<Cell> allCells, Cell cell)
        {
            var adjacentCells = allCells.Where(thisCell => Math.Abs(thisCell.X - cell.X) <= 1 && Math.Abs(thisCell.Y - cell.Y) <= 1).ToList();

            return adjacentCells;
        }

        public class Cell
        {
            public Cell(int x, int y)
            {
                X = x;
                Y = y;
            }

            public Cell(int x, int y, int value) : this(x, y)
            {
                Value = value;
            }

            public int X { get; private set; }

            public int Y { get; private set; }

            public int Value { get; set; }
        }
    }
}
