﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day06 : AdventDay, IAdventDay
    {
        private List<int[]> _history;
        private int _distributionCount;

        public Day06() : base(2017, 6)
        {
        }

        public string CalculatePartA()
        {
            return CalculateDistributions(GetInput(), HistoryContainsDuplicatesOfLast).ToString();
        }

        public string CalculatePartB()
        {
            var firstLoop = CalculateDistributions(GetInput(), HistoryContainsDuplicatesOfLast);
            var secondSeed = _history.Last().Clone() as int[];
            var secondLoop = CalculateDistributions(secondSeed, () => HistoryContainsDuplicatesOfValue(secondSeed));

            return secondLoop.ToString();
        }

        public int CalculateDistributions(int[] input, Func<bool> completeCalculator)
        {
            _distributionCount = 0;
            _history = new List<int[]>();
            _history.Add(input.Clone() as int[]);

            while (!completeCalculator())
            {
                var indexOfMax = GetIndexOfMax(input);
                input = RedistributeIndex(input, indexOfMax);
                _history.Add(input.Clone() as int[]);
                _distributionCount++;

            }

            return _distributionCount;
        }

        private bool HistoryContainsDuplicatesOfValue(int[] value)
        {
            return _history.Count(h => h.SequenceEqual(value)) > 1;
        }

        private bool HistoryContainsDuplicatesOfLast()
        {
            return _history.Count(h => h.SequenceEqual(_history.Last())) > 1;
        }

        private int GetIndexOfMax(int[] array)
        {
            return Array.IndexOf(array, array.Max());
        }

        private int[] RedistributeIndex(int[] originalArray, int index)
        {
            var clonedArray = originalArray.Clone() as int[];
            int value = clonedArray[index];
            clonedArray[index] = 0;

            int indexToIncrement = index == clonedArray.Length - 1 ? 0 : index + 1;

            Enumerable.Range(1, value)
                      .ToList()
                      .ForEach(i => {
                          clonedArray[indexToIncrement] = clonedArray[indexToIncrement] + 1;
                          indexToIncrement = indexToIncrement == clonedArray.Length - 1 ? 0 : indexToIncrement + 1;
                      });

            return clonedArray;

        }

        private int[] GetInput()
        {
            var inputString = "4	1	15	12	0	9	9	5	5	8	7	3	14	5	12	3";

            var number = new Regex("[0-9]+");

            return number.Matches(inputString)
                         .Cast<Match>()
                         .Select(m => int.Parse(m.Value))
                         .ToArray();
        }
    }
}
