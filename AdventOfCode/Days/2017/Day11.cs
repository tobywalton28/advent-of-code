﻿using System;

namespace AdventOfCode.Days._2017
{
    public class Day11 : AdventDay, IAdventDay
    {
        public Day11() : base(2017, 11)
        {
        }

        private int _North = 0;
        private int _East = 0;
        private int _FurthestDistance = 0;

        private void ResetPosition()
        {
            _North = 0;
            _East = 0;
            _FurthestDistance = 0;
        }

        public string CalculatePartA()
        {
            ResetPosition();
            var input = GetInput();

            foreach (string direction in input)
            {
                ProcessDirection(direction);
            }

            return CalculateSteps().ToString();
        }

        public string CalculatePartB()
        {
            ResetPosition();
            var input = GetInput();

            foreach (string direction in input)
            {
                ProcessDirection(direction);
                int stepsToStart = CalculateSteps();
                _FurthestDistance = Math.Max(_FurthestDistance, stepsToStart);
            }

            return _FurthestDistance.ToString();
        }

        private int CalculateSteps()
        {
            var distance = (Math.Abs(_East) + Math.Abs(_North) + Math.Abs(_North + _East)) / 2;

            return distance;
        }

        private void ProcessDirection(string direction)
        {
            switch (direction)
            {
                case "n":
                    _North += 1;
                    break;
                case "s":
                    _North -= 1;
                    break;
                case "ne":
                    _East += 1;
                    break;
                case "nw":
                    _North += 1;
                    _East -= 1;
                    break;
                case "se":
                    _North -= 1;
                    _East += 1;
                    break;
                case "sw":
                    _East -= 1;
                    break;
            }
        }

        private string[] GetInput()
        {            
            return base.GetInput()[0].Split(',');
        }
    }
}
