﻿using System;
using System.Linq;

namespace AdventOfCode.Days._2017
{
    public class Day01 : AdventDay, IAdventDay
    {
        public Day01() : base(2017, 1)
        {
        }

        public string CalculatePartA()
        {
            Func<int, int, int> indexCalculator = (charCount, index) => {
                return index == charCount - 1 ? 0 : index + 1;
            };

            return CalculateCaptcha(GetInput()[0], indexCalculator).ToString();
        }

        public string CalculatePartB()
        {
            Func<int, int, int> indexCalculator = (charCount, index) => {
                int newIndex = index + charCount / 2;
                if (newIndex > charCount - 1) newIndex -= charCount;
                return newIndex;
            };

            return CalculateCaptcha(GetInput()[0], indexCalculator).ToString();
        }

        private int CalculateCaptcha(string input, Func<int, int, int> indexCalculator)
        {
            var charArray = input.ToCharArray();

            var result = charArray
                .Select((character, index) => new {
                    Value = int.Parse(character.ToString()),
                    NextValue = int.Parse(charArray[indexCalculator(charArray.Length, index)].ToString())
                })
                .Where(data => data.Value == data.NextValue)
                .Sum(data => data.Value);

            return result;
        }
    }
}
