﻿using System;
using System.Linq;

namespace AdventOfCode.Days._2017
{
    public class Day14 : AdventDay, IAdventDay
    {
        public Day14() : base(2017, 14)
        {
        }

        public const string input = "stpzcrnm";

        public string CalculatePartA()
        {

            var knotHash = new KnotHash();
            var foo = knotHash.Calculate("AoC 2017");

            int usedSquares = Enumerable.Range(0, 128)
                .Select(rowNumber => input + "-" + rowNumber)
                .Select(s => knotHash.Calculate(s))
                .Select(s => string.Join(string.Empty, s.ToCharArray().Select(ch => HexToBinary(ch.ToString()))))
                .Select(s => s.Count(ch => ch == '1'))
                .Sum();

            return usedSquares.ToString();
        }

        public string CalculatePartB()
        {
            return string.Empty;
        }

        string HexToBinary(string hexString)
        {
            string binaryval = "";
            string tmp = Convert.ToString(Convert.ToInt32(hexString, 16), 2);
            binaryval = String.Format("{0:X4}", tmp).PadLeft(4, '0');
            return binaryval;
        }
    }
}
