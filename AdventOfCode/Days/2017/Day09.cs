﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day09 : AdventDay, IAdventDay
    {
        public Day09() : base(2017, 9)
        {
        }

        private int _garbageCount = 0;

        public string CalculatePartA()
        {
            var input = GetInput()[0];

            input = ProcessCancellations(input);
            input = ProcessGarbage(input);
            int score = CalculateGroupScore(input);

            return score.ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput()[0];

            input = ProcessCancellations(input);
            input = ProcessGarbage(input);
            int score = CalculateGroupScore(input);

            return _garbageCount.ToString();
        }

        private string ProcessCancellations(string input)
        {
            Regex calcelationRegex = new Regex("[!]+[^!]");

            string result = calcelationRegex.Replace(input, delegate (Match m) {

                //theres only one calcellation, which cancels the other character
                if (m.Value.Length == 2)
                    return string.Empty;

                //there are an odd number of cancellations - all characters cancelled
                if (m.Value.ToCharArray().Count(c => c == '!') % 2 == 1)
                    return string.Empty;

                //return last character
                return m.Value.Replace("!", string.Empty);

                //throw new Exception($"not sure how to hadle '{m.Value}'");


            });

            return result;
        }

        private string ProcessGarbage(string input)
        {
            Regex garbageRegex = new Regex("<[^>]*>");
            _garbageCount = 0;

            while (garbageRegex.IsMatch(input))
            {
                var nextGarbage = garbageRegex.Match(input);
                _garbageCount += nextGarbage.Value.Length - 2;
                input = garbageRegex.Replace(input, string.Empty, 1);
            }

            return input;
        }

        private int CalculateGroupScore(string input)
        {
            int currentLevel = 0;
            int totalScore = 0;

            foreach (var charachter in input.ToCharArray())
            {
                switch (charachter)
                {
                    case '{':
                        currentLevel += 1;
                        break;
                    case '}':
                        totalScore += currentLevel;
                        currentLevel -= 1;
                        break;
                }
            }

            return totalScore;
        }
    }
}
