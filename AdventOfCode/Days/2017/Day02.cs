﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day02 : AdventDay, IAdventDay
    {
        public Day02() : base(2017, 2)
        {
        }

        public string CalculatePartA()
        {
            Regex number = new Regex("[0-9]+");            

            var linesAndValues = GetInput().Select(line => number.Matches(line))
                .Select(matches => matches.Cast<Match>().Select(m => int.Parse(m.Value)).ToList())
                .ToList();

            var diffs = linesAndValues.Select(values => new {
                Min = values.Min(),
                Max = values.Max(),
                Diff = values.Max() - values.Min()
            });

            return diffs.Sum(d => d.Diff).ToString();
        }

        public string CalculatePartB()
        {
            Regex number = new Regex("[0-9]+");
            
            var linesAndValues = GetInput().Select(line => number.Matches(line))
                .Select(matches => matches.Cast<Match>().Select(m => int.Parse(m.Value)).ToList())
                .ToList();

            var factors = linesAndValues.Select(line => CalculateFactors(line.ToArray())).ToList();

            var totals = factors.Select(factor => factor[0] / factor[1]).ToList();

            return totals.Sum().ToString();
        }

        private List<int> CalculateFactors(int[] values)
        {
            for (int outerIndex = 0; outerIndex < values.Length; outerIndex++)
            {
                for (int innerIndex = 0; innerIndex < values.Length; innerIndex++)
                {
                    if (innerIndex == outerIndex) continue;

                    decimal divisionResult = values[outerIndex] / (decimal)values[innerIndex];
                    if (divisionResult % 1 == 0)
                    {
                        return new List<int> {
                            values[outerIndex],
                            values[innerIndex]
                        };
                    }
                }
            }

            return new List<int>();
        }
    }
}
