﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day08 : AdventDay, IAdventDay
    {
        public Day08() : base(2017, 8)
        {
        }

        private int _maxRegisterValue;

        public string CalculatePartA()
        {
            var registerValues = CalculateAllRegisters();

            return registerValues.Max(reg => reg.Value).ToString();
        }

        public string CalculatePartB()
        {
            var registerValues = CalculateAllRegisters();

            return _maxRegisterValue.ToString();
        }

        public Dictionary<string, int> CalculateAllRegisters()
        {
            var input = GetInput().ToList();

            var allRegisters = input.Select(line => line.TargetRegisterName)
                                             .Distinct()
                                             .ToDictionary(name => name, name => 0);

            foreach (var line in input)
            {
                int updatedRegisterValue = ProcessInstruction(line, allRegisters[line.TargetRegisterName], allRegisters[line.ComparisonRegisterName]);
                _maxRegisterValue = Math.Max(_maxRegisterValue, updatedRegisterValue);
                allRegisters[line.TargetRegisterName] = updatedRegisterValue;
            }

            return allRegisters;
        }

        private int ProcessInstruction(Instruction instruction, int originalTargetRegisterValue, int comparisonRegisterValue)
        {
            var conditionMet = EvaluateCondition(instruction, comparisonRegisterValue);

            if (conditionMet)
            {
                if (instruction.Operation == "inc") return originalTargetRegisterValue + instruction.OperationValue;
                if (instruction.Operation == "dec") return originalTargetRegisterValue - instruction.OperationValue;
            }

            return originalTargetRegisterValue;
        }

        private bool EvaluateCondition(Instruction instruction, int comparisonRegisterValue)
        {
            switch (instruction.ComparisonType)
            {
                case ">=":
                    return comparisonRegisterValue >= instruction.ComparisonValue;
                case ">":
                    return comparisonRegisterValue > instruction.ComparisonValue;
                case "<":
                    return comparisonRegisterValue < instruction.ComparisonValue;
                case "!=":
                    return comparisonRegisterValue != instruction.ComparisonValue;
                case "==":
                    return comparisonRegisterValue == instruction.ComparisonValue;
                case "<=":
                    return comparisonRegisterValue <= instruction.ComparisonValue;
                default:
                    throw new Exception($"unknown operator {instruction.ComparisonType}");
            }
        }

        private IEnumerable<Instruction> GetInput()
        {            
            Regex nonWhitespace = new Regex(@"\S+");

            foreach (var line in base.GetInput())
            {
                var matches = nonWhitespace.Matches(line);

                yield return new Instruction
                {
                    TargetRegisterName = matches[0].Value,
                    Operation = matches[1].Value,
                    OperationValue = int.Parse(matches[2].Value),
                    ComparisonRegisterName = matches[4].Value,
                    ComparisonType = matches[5].Value,
                    ComparisonValue = int.Parse(matches[6].Value)
                };
            }
        }

        private class Instruction
        {
            public string TargetRegisterName { get; set; }

            public string Operation { get; set; }

            public int OperationValue { get; set; }

            public string ComparisonRegisterName { get; set; }

            public string ComparisonType { get; set; }

            public int ComparisonValue { get; set; }
        }
    }
}
