﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day07 : AdventDay, IAdventDay
    {
        public Day07() : base(2017, 7)
        {
        }

        public string CalculatePartA()
        {
            var input = GetInput();
            var allNodes = GetAllNodes(input);
            var root = FindRoot(allNodes);

            return root.Name;
        }

        public string CalculatePartB()
        {
            var input = GetInput();
            var allNodes = GetAllNodes(input);
            var root = FindRoot(allNodes);

            var unbalancedNode = FindUnbalancedNodeRecursive(root);
            var adjustment = CalculateBalanceAdjustment(root);

            return (unbalancedNode.Weight + adjustment).ToString();
        }      

        private List<Node> GetAllNodes(string[] input)
        {
            var nameRegex = new Regex(@"\S+");
            var weightRegex = new Regex(@"(\d+)");

            var allNodes = new List<Node>();

            input.ToList().ForEach(line => {
                string name = nameRegex.Match(line).Value;
                int weight = int.Parse(weightRegex.Match(line).Value);
                string[] descendantsString = line.Contains("->") ? line.Substring(line.IndexOf("->")).Replace("->", string.Empty).Split(',').Select(s => s.Trim()).ToArray() : new string[0];

                allNodes.Add(new Node(name, weight, descendantsString));
            });

            foreach (var node in allNodes)
                FindNodeDescendants(node, allNodes);

            return allNodes;
        }

        private void FindNodeDescendants(Node node, List<Node> allNodes)
        {
            foreach (string descendantName in node.DescendantsString)
            {
                Node descendant = allNodes.Single(thisNode => thisNode.Name == descendantName);
                node.AddDescendant(descendant);
            }
        }

        private int CalculateBalanceAdjustment(Node root)
        {
            var branchWeights = root.Descendants.Select(d => d.GetBranch().Sum(br => br.Weight));
            var branchWeightGroups = branchWeights.GroupBy(weight => weight);

            int correctBranchWeight = branchWeightGroups.Single(grp => grp.Count() > 1).Key;
            int incorrectBranchWeight = branchWeightGroups.Single(grp => grp.Count() == 1).Key;

            return correctBranchWeight - incorrectBranchWeight;
        }

        private Node FindUnbalancedNode(Node parent)
        {
            var branchWeights = parent.Descendants.Select(sibling => new {
                Node = sibling,
                BranchWeight = sibling.GetBranch().Sum(node => node.Weight)
            }).GroupBy(bw => bw.BranchWeight);

            var unbalancedNode = branchWeights.SingleOrDefault(bw => bw.Count() == 1);

            if (unbalancedNode == null)
                return parent;

            return unbalancedNode.Single().Node;
        }

        private Node FindUnbalancedNodeRecursive(Node node)
        {
            bool nodeChange = true;
            string lastNodeName = node.Name;

            while (nodeChange)
            {
                node = FindUnbalancedNode(node);
                nodeChange = lastNodeName != node.Name;
                lastNodeName = node.Name;
            }

            return node;
        }

        private Node FindRoot(List<Node> allNodes)
        {
            var rootNodes = allNodes.Where(thisNode => !allNodes.Any(thatNode => thatNode.HasDescendant(thisNode.Name)));

            if (!rootNodes.Any()) throw new Exception("could not find root node");
            if (rootNodes.Count() > 1) throw new Exception("found multiple root nodes");

            return rootNodes.Single();
        }

        private class Node
        {
            public Node(string name, int weight, string[] descendantsString)
            {
                Name = name;
                Weight = weight;
                DescendantsString = descendantsString;
                Descendants = new List<Node>();
            }

            public string Name { get; private set; }

            public int Weight { get; private set; }

            public string[] DescendantsString { get; private set; }

            public List<Node> Descendants { get; private set; }

            public void AddDescendant(Node descendant)
            {
                Descendants.Add(descendant);
            }

            public bool HasDescendant(string name)
            {
                return Descendants.Any(d => d.Name == name);
            }

            public List<Node> GetBranch()
            {
                var branch = new List<Node>();
                branch.Add(this);
                branch.AddRange(Descendants.Flatten(d => d.Descendants));

                return branch;
            }
        }
    }
}
