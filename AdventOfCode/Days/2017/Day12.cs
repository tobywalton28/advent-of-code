﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day12 : AdventDay, IAdventDay
    {
        public Day12() : base(2017, 12)
        {
        }

        private Dictionary<int, Program> _programs;

        public string CalculatePartA()
        {
            var input = GetInput();
            _programs = GetPrograms(input);
            var group0 = GetConnectionsForProgram(0);

            return group0.Length.ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput();
            _programs = GetPrograms(input);

            var allProgramIds = _programs.Keys.ToArray();
            List<int> foundProgramIds = new List<int>();
            int groupCount = 0;

            while (foundProgramIds.Count() < allProgramIds.Length)
            {
                var groupSeed = allProgramIds.First(id => !foundProgramIds.Contains(id));
                var groupConnections = GetConnectionsForProgram(groupSeed);
                foundProgramIds.AddRange(groupConnections);
                groupCount++;
            }

            return groupCount.ToString();
        }

        private int[] GetConnectionsForProgram(int id)
        {
            List<int> allConnectons = new List<int> { id };
            bool foundMoreConnections = true;
            var program = _programs[id];
            allConnectons.AddRange(program.Connections.Where(c => !allConnectons.Contains(c)));

            while (foundMoreConnections)
            {
                foundMoreConnections = false;
                List<int> newConnections = new List<int>();

                foreach (var thisConnection in allConnectons)
                {
                    var thisProgram = _programs[thisConnection];
                    newConnections.AddRange(thisProgram.Connections.Where(c => !allConnectons.Contains(c)));
                }

                if (newConnections.Any())
                {
                    foundMoreConnections = true;
                    allConnectons.AddRange(newConnections);
                }
            }

            return allConnectons.ToArray();
        }       

        private Dictionary<int, Program> GetPrograms(string[] input)
        {
            var idRegex = new Regex(@"\d+");
            var connectionsRegex = new Regex(@"<->.*$");

            return input.Select(line =>
            {
                var program = new Program
                {
                    Id = int.Parse(idRegex.Match(line).Value),
                    ConnectionString = connectionsRegex.Match(line).Value.Replace("<->", string.Empty).Trim(),
                    Connections = new int[0]
                };

                program.Connections = program.ConnectionString.Split(',').Select(s => int.Parse(s)).ToArray();

                return program;
            }).ToDictionary(p => p.Id);
        }

        private class Program
        {
            public int Id { get; set; }

            public string ConnectionString { get; set; }

            public int[] Connections { get; set; }
        }
    }
}
