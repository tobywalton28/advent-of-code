﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2017
{
    public class Day04 : AdventDay, IAdventDay
    {
        public Day04() : base(2017, 4)
        {
        }

        public string CalculatePartA()
        {
            string[] input = GetInput();

            var passphrase = new Regex(@"\S+");

            int result = input.Select(line => passphrase.Matches(line).Cast<Match>().Select(m => m.Value).ToList())
                .Select(x => x.Count() == x.Distinct().Count())
                .Count(v => v == true);

            return result.ToString();
        }

        public string CalculatePartB()
        {
            string[] input = GetInput();

            var passphrase = new Regex(@"\S+");

            int result = input.Select(line => passphrase.Matches(line).Cast<Match>().Select(m => m.Value).ToList())
                .Where(p => PassPhraseValid(p))
                .Count();

            return result.ToString();
        }

        private bool PassPhraseValid(List<string> passPhrase)
        {
            return passPhrase.Count() == passPhrase.Distinct().Count() && !PassPhraseContainsAnagrams(passPhrase);
        }

        private bool PassPhraseContainsAnagrams(List<string> passPhrase)
        {
            var passPhraseArray = passPhrase.ToArray();

            for (int outerIndex = 0; outerIndex < passPhraseArray.Length; outerIndex++)
            {
                for (int innerIndex = 0; innerIndex < passPhraseArray.Length; innerIndex++)
                {
                    if (outerIndex == innerIndex) continue;
                    if (IsAnagram(passPhraseArray[outerIndex], passPhraseArray[innerIndex])) return true;
                }
            }

            return false;
        }

        private bool IsAnagram(string stringA, string stringB)
        {
            var aSorted = new string(stringA.ToCharArray().OrderBy(c => c).ToArray());
            var bSorted = new string(stringB.ToCharArray().OrderBy(c => c).ToArray());

            return aSorted == bSorted;
        }     
    }
}
