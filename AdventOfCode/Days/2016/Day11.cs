﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2016
{
    public class Day11 : AdventDay, IAdventDay
    {
        public Day11() : base(2016, 11)
        {
            
        }

        public string CalculatePartA()
        {
            var building = new Dictionary<int, int> {
                { 4, 0 },
                { 3, 1 },
                { 2, 5 },
                { 1, 4 }
            };

            return CalculateLiftMoves(building).ToString();
        }

        public string CalculatePartB()
        {
            var building = new Dictionary<int, int> {
                { 4, 0 },
                { 3, 1 },
                { 2, 5 },
                { 1, 8 }
            };

            return CalculateLiftMoves(building).ToString();
        }

        private int CalculateLiftMoves(Dictionary<int, int> building)
        {
            int liftMoves = MoveFirstTwoItemsToTop(building);
            liftMoves += MoveRemainingItemsToTop(building);
            
            return liftMoves;
        }

        private int MoveFirstTwoItemsToTop(Dictionary<int, int> building)
        {
            int topFloor = building.Keys.Max();

            building[1] -= 2;
            building[topFloor] += 2;

            return building.Keys.Max() - 1;
        }

        private int MoveRemainingItemsToTop(Dictionary<int, int> building)
        {
            int liftMoves = 0;
            int topFloor = building.Keys.Max();

            for (int floor = 1; floor < topFloor; floor++)
            {
                int itemsOnFloor = building[floor];
                liftMoves += itemsOnFloor * (2 * (topFloor - floor));
            }

            return liftMoves;
        }
    }
}
