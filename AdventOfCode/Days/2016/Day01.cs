﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2016
{
    public class Day01 : AdventDay, IAdventDay
    {
        private int _northDistance;
        private int _eastDistance;
        private char _heading = 'N';
        private char[] _headings = new[] {'N', 'E', 'S', 'W'};
        private Dictionary<string, int> _progress = new Dictionary<string, int>();
        private int? _partB;

        public Day01() : base(2016, 1)
        {
        }

        public string CalculatePartA()
        {            
            var directions = GetInput()[0].Split(',').Select(str => str.Trim()).Select(dir => new
            {
                Direction = dir[0],
                Distance = int.Parse(dir.Substring(1))
            }).ToList();

            LogPosition();

            foreach (var dir in directions)
            {
                Turn(dir.Direction);
                Move(dir.Distance);
            }

            return (Math.Abs(_northDistance) + Math.Abs(_eastDistance)).ToString();
        }

        public string CalculatePartB()
        {
            return _partB.Value.ToString();
        }

        private void Turn(char direction)
        {
            var currentIndex = _headings.ToList().IndexOf(_heading);
            var newIndex = direction == 'R' ? currentIndex + 1 : currentIndex - 1;

            if (newIndex == -1) _heading = _headings.Last();
            else if (newIndex == 4) _heading = _headings.First();
            else _heading = _headings[newIndex];
        }

        private void Move(int distance)
        {
            for (int i = 1; i <= distance; i++)
            {
                switch (_heading)
                {
                    case 'N':
                        _northDistance += 1;
                        break;
                    case 'S':
                        _northDistance -= 1;
                        break;
                    case 'E':
                        _eastDistance += 1;
                        break;
                    case 'W':
                        _eastDistance -= 1;
                        break;
                }

                LogPosition();
            }         
        }

        private void LogPosition()
        {
            string key = $"{_northDistance},{_eastDistance}";
            if (!_progress.ContainsKey(key)) _progress.Add(key, 0);
            _progress[key] += 1;

            if (_progress[key] == 2 && !_partB.HasValue)
            {
                _partB = Math.Abs(_northDistance) + Math.Abs(_eastDistance);
            }
        }
    }
}
