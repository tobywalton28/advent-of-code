﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2016
{
    public class Day06 : AdventDay, IAdventDay
    {
        public Day06() : base(2016, 6) { }

        public string CalculatePartA()
        {
            Func<Dictionary<char, int>, char> mostCommonChar = dictionary =>
            {
                return dictionary.OrderByDescending(dic => dic.Value)
                                 .First()
                                 .Key;
            };

            return CalculateMessage(mostCommonChar);
        }

        public string CalculatePartB()
        {
            Func<Dictionary<char, int>, char> leastCommonChar = dictionary =>
            {
                return dictionary.OrderBy(dic => dic.Value)
                                 .First()
                                 .Key;
            };

            return CalculateMessage(leastCommonChar);
        }

        private string CalculateMessage(Func<Dictionary<char, int>, char> charCalculation)
        {
            var input = GetInput().Select(str => str.ToCharArray()).ToList();
            var length = input.First().Length;
            string result = string.Empty;

            for (int i = 0; i < length; i++)
            {
                var charDictionary = input.Select(c => c[i])
                    .GroupBy(c => c)
                    .ToDictionary(grp => grp.Key, grp => grp.Count());

                result += charCalculation(charDictionary);
            }

            return result;
        }
    }
}
