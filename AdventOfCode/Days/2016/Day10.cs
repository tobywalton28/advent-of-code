﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2016
{
    public class Day10 : AdventDay, IAdventDay
    {
        private const string BOT_PATTERN = @"bot \d+";
        private const string OUTPUT_PATTERN = @"output \d+";
        private const string INSTRUCTION_PATTERN = @"bot \d+ gives low to .+ and high to .+";
        private const string TYPE_AND_NUMBER_PATTERN = @"\w+ \d+";
        private const string VALUE_INIT_PATTERN = @"value \d+ goes to bot \d+";

        private List<Bot> _bots;
        private List<Bin> _bins;        

        public Day10() : base(2016, 10)
        {
            
        }

        public string CalculatePartA()
        {
            RunSimulation();

            return _bots.Single(b => b.Result != null).Result;
        }

        public string CalculatePartB()
        {
            RunSimulation();

            var bin0 = _bins.Single(b => b.Number == 0).Chips.Single();
            var bin1 = _bins.Single(b => b.Number == 1).Chips.Single();
            var bin2 = _bins.Single(b => b.Number == 2).Chips.Single();

            return (bin0 * bin1 * bin2).ToString();
        }


        private void RunSimulation()
        {
            var input = GetInput();

            _bots = input.SelectMany(str => Regex.Matches(str, BOT_PATTERN).Cast<Match>())
                            .Select(botStr => int.Parse(botStr.Value.Replace("bot", string.Empty)))
                            .Distinct()
                            .Select(botNumber => new Bot(botNumber))
                            .ToList();

            _bins = input.SelectMany(str => Regex.Matches(str, OUTPUT_PATTERN).Cast<Match>())
                                  .Select(outputStr => int.Parse(outputStr.Value.Replace("output", string.Empty)))
                                  .Distinct()
                                  .Select(i => new Bin(i))
                                  .ToList();

            var instructions = input.SelectMany(str => Regex.Matches(str, INSTRUCTION_PATTERN).Cast<Match>())
                                    .Select(m => Regex.Matches(m.Value, TYPE_AND_NUMBER_PATTERN))
                                    .ToList();

            var initialisation = input.SelectMany(str => Regex.Matches(str, VALUE_INIT_PATTERN).Cast<Match>())
                                      .Select(m => Regex.Matches(m.Value, @"\d+").Cast<Match>().Select(num => int.Parse(num.Value)).ToArray())
                                      .Select(y => new
                                      {
                                          Bot = y[1],
                                          Number = y[0]
                                      })
                                      .ToList();

            foreach (var instruction in instructions)
            {
                var botNumber = int.Parse(instruction[0].Value.Replace("bot", string.Empty));
                var bot = _bots.Single(b => b.Number == botNumber);
                bot.LowRecipient = FindRecipient(instruction[1].Value);
                bot.HighRecipient = FindRecipient(instruction[2].Value);
            }

            foreach (var init in initialisation)
            {
                var bot = _bots.Single(b => b.Number == init.Bot);
                bot.Receive(init.Number);
            }
        }

        private IRecipient FindRecipient(string recipient)
        {
            bool isBot = recipient.Contains("bot");
            var number = int.Parse(recipient.Replace("bot", string.Empty).Replace("output", string.Empty));

            return isBot ? (IRecipient)_bots.Single(b => b.Number == number) 
                         : (IRecipient)_bins.Single(b => b.Number == number);

        }

        private interface IRecipient
        {
            void Receive(int number);
        }

        private class Bot : IRecipient
        {
            public Bot(int number)
            {
                Number = number;
                Chips = new List<int>();
            }

            public string Result { get; private set; }

            public int Number { get; }

            public List<int> Chips { get; private set; }

            public IRecipient LowRecipient { get; set; }

            public IRecipient HighRecipient { get; set; }

            public void Receive(int number)
            {
                Chips.Add(number);
                if (Chips.Count == 2)
                {
                    PassChips();
                }
            }

            public bool HasChips(int chipA, int chipB)
            {
                return Chips.Contains(chipA) && Chips.Contains(chipB);
            }

            public bool HasBothChips()
            {
                return Chips.Count == 2;
            }

            public void PassChips()
            {
                if (HasBothChips() == false) throw new Exception("I do not have 2 chips to pass");

                if (Chips.Contains(17) && Chips.Contains(61))
                {
                    Result = Number.ToString();
                }

                LowRecipient.Receive(Chips.Min());
                HighRecipient.Receive(Chips.Max());
                Chips = new List<int>();
            }
        }

        private class Bin : IRecipient
        {
            public Bin(int number)
            {
                Number = number;
                Chips = new List<int>();
            }

            public int Number { get; }

            public List<int> Chips { get; }

            public void Receive(int number)
            {
                Chips.Add(number);
            }
        }
    }
}
