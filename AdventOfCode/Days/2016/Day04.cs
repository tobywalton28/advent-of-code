﻿using System;
using System.Linq;

namespace AdventOfCode.Days._2016
{
    public class Day04 : AdventDay, IAdventDay
    {
        public Day04() : base(2016, 4)
        {
        }

        private readonly char[] _alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        public string CalculatePartA()
        {
            var input = GetInput();

            var rooms = input.Select(str => new {
                EncryptedName = str.Substring(0, str.LastIndexOf('-')),
                SectorId = int.Parse(str.Substring(str.LastIndexOf('-') + 1, str.IndexOf('[') - str.LastIndexOf('-') - 1)),
                Checksum = str.Substring(str.IndexOf('[')).Replace("[", string.Empty).Replace("]", string.Empty)
            }).ToArray();

            var realRooms = rooms.Where(r => RoomIsReal(r.EncryptedName, r.Checksum))
                                 .Sum(r => r.SectorId);

            return realRooms.ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput();

            var rooms = input.Select(str => new {
                EncryptedName = str.Substring(0, str.LastIndexOf('-')),
                SectorId = int.Parse(str.Substring(str.LastIndexOf('-') + 1, str.IndexOf('[') - str.LastIndexOf('-') - 1)),
                Checksum = str.Substring(str.IndexOf('[')).Replace("[", string.Empty).Replace("]", string.Empty)
            }).ToArray();

            var realRooms = rooms.Where(r => RoomIsReal(r.EncryptedName, r.Checksum));


            var decryptedRooms = realRooms.Select(r => 
                new {
                    DecryptedName = DecryptName(r.EncryptedName, r.SectorId),
                    SectorId = r.SectorId
                }
            ).ToArray();

            return decryptedRooms.Single(r => r.DecryptedName.Contains("northpole")).SectorId.ToString();
        }

        private bool RoomIsReal(string encryptedName, string checksum)
        {
            string hyphenFreeName = encryptedName.Replace("-", string.Empty);

            var characterCount = hyphenFreeName.ToCharArray()
                                               .GroupBy(arr => arr)
                                               .Select(grp => new { Character = grp.Key, Count = grp.Count() })
                                               .OrderByDescending(c => c.Count)
                                               .ThenBy(c => c.Character)
                                               .ToArray();

            var sortedName = new string(characterCount.Take(checksum.Length)
                                    .Select(c => c.Character)
                                    .ToArray());

            return sortedName == checksum;
        }

        private string DecryptName(string encryptedName, int shift)
        {
            return new string(encryptedName.ToCharArray()
                                           .Select(ch => { return ch == '-' ? ' ' : ShiftCharacter(ch, shift); })
                                           .ToArray());
        }

        private char ShiftCharacter(char input, int shift)
        {
            var index = Array.IndexOf(_alphabet, input);
            var newIndex = index + shift;

            while (newIndex > _alphabet.Length - 1)
            {
                newIndex -= _alphabet.Length;
            }

            return _alphabet[newIndex];
        }
    }

}
