﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2016
{
    public class Day09 : AdventDay, IAdventDay
    {
        private const string MARKER_FORMAT = @"\(\d+x\d+\)";
        private const string NUMBER_FORMAT = @"\d+";

        public Day09() : base(2016, 9)
        {            
        }

        public string CalculatePartA()
        {
            var input = GetInput()[0];

            return Decompress(input, false).ToString();            
        }

        public string CalculatePartB()
        {
            var input = GetInput()[0];

            return Decompress(input, true).ToString();
        }

        private Int64 Decompress(string input, bool recursive)
        {            
            var match = Regex.Match(input, MARKER_FORMAT);
            Int64 decompressedLength = 0;

            while (match.Success)
            {                
                decompressedLength += match.Index;
                input = input.Substring(match.Index + match.Length);
                var numbers = Regex.Matches(match.Value, NUMBER_FORMAT).Cast<Match>().Select(m => int.Parse(m.Value)).ToArray();
                int characters = numbers[0];
                int repetition = numbers[1];
                string charactersToRepeat = input.Substring(0, characters);
                string generatedCharacters = string.Concat(Enumerable.Repeat(charactersToRepeat, repetition));
                
                if (recursive)
                {
                    input = generatedCharacters + input.Substring(characters);
                }
                else
                {
                    input = input.Substring(characters);
                    decompressedLength += generatedCharacters.Length;                                        
                }

                match = Regex.Match(input, MARKER_FORMAT);
            }

            decompressedLength += input.Length;

            return decompressedLength;
        }







        //private string Decompress(string input, bool decompressMarkers)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    var match = Regex.Match(input, MARKER_FORMAT);

        //    while (match.Success)
        //    {
        //        sb.Append(input.Substring(0, match.Index));
        //        input = input.Substring(match.Index + match.Length);
        //        var numbers = Regex.Matches(match.Value, NUMBER_FORMAT).Cast<Match>().Select(m => int.Parse(m.Value)).ToArray();                
        //        int characters = numbers[0];
        //        int repetition = numbers[1];
        //        string charactersToRepeat = input.Substring(0, characters);
        //        string generatedCharacters = string.Concat(Enumerable.Repeat(charactersToRepeat, repetition));
        //        sb.Append(generatedCharacters);
        //        input = input.Substring(characters);
        //        match = Regex.Match(input, MARKER_FORMAT);
        //    }

        //    sb.Append(input);

        //    var result = sb.ToString();

        //    if (decompressMarkers && Regex.IsMatch(result, MARKER_FORMAT))
        //    {
        //        return Decompress(result, true);
        //    }

        //    return result;
        //}
    }
}
