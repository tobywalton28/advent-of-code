﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2016
{
    public class Day07 : AdventDay, IAdventDay
    {
        private const string HYPERNET_PATTERN = @"\[\w+\]";

        public Day07() : base(2016, 7)
        {
        }

        public string CalculatePartA()
        {
            return GetInput().Count(SupportsTLS).ToString();
        }

        public string CalculatePartB()
        {
            return GetInput().Count(SupportsSSL).ToString();
        }

        private bool SupportsTLS(string ip)
        {
            string[] ipWithoutHypernets = RemoveHypernets(ip);
            string[] hypernets = GetHypernets(ip);

            return ipWithoutHypernets.Any(ContainsABBA) && !hypernets.Any(ContainsABBA);
        }

        private bool SupportsSSL(string ip)
        {
            string[] ipWithoutHypernets = RemoveHypernets(ip);
            string[] hypernets = GetHypernets(ip);

            var hypernetABA = hypernets.SelectMany(str => Get_ABA_And_BAB(str)).ToList();
            var ipBAB = ipWithoutHypernets.SelectMany(str => Get_ABA_And_BAB(str)).ToList();

            return hypernetABA.Any(f => ipBAB.Exists(b => b[0] == f[1] && b[1] == f[0] && b[2] == f[1]));
        }

        private string[] GetHypernets(string ip)
        {
            return Regex.Matches(ip, HYPERNET_PATTERN)
                        .Cast<Match>()
                        .Select(m => m.Value.Replace("[", string.Empty).Replace("]", string.Empty))
                        .ToArray();            
        }

        private string[] RemoveHypernets(string ip)
        {                        
            return Regex.Replace(ip, HYPERNET_PATTERN, ",").Split(',');
        }

        private bool ContainsABBA(string input)
        {
            var startIndexes = Enumerable.Range(0, input.Length - 3).ToList();
            var groupsOfFourChars = startIndexes.Select(i => input.Substring(i, 4)).ToList();

            return groupsOfFourChars.Any(IsABBA);
        }

        private bool IsABBA(string fourCharacterSequence)
        {
            return fourCharacterSequence[0] == fourCharacterSequence[3] 
                   &&
                   fourCharacterSequence[1] == fourCharacterSequence[2] 
                   && 
                   fourCharacterSequence.ToCharArray().Distinct().Count() != 1;
        }

        private string[] Get_ABA_And_BAB(string input)
        {
            var startIndexes = Enumerable.Range(0, input.Length - 2).ToList();
            var groupsOfThreeChars = startIndexes.Select(i => input.Substring(i, 3)).ToList();

            return groupsOfThreeChars.Where(str => str[0] == str[2] && str[0] != str[1]).ToArray();
        }
    }
}
