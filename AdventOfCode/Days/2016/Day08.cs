﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2016
{
    public class Day08 : AdventDay, IAdventDay
    {
        public Day08() : base(2016, 8)
        {

        }

        private int _height;
        private int _width;
        private bool[,] _display;

        public string CalculatePartA()
        {
            InitialiseDisplay(50, 6);
            ProcessInstructions();

            var pixels = _display.Cast<bool>().ToArray();

            return pixels.Count(p => p).ToString();
        }

        public string CalculatePartB()
        {
            InitialiseDisplay(50, 6);
            ProcessInstructions();

            return DisplayScreen();
        }

        private string DisplayScreen()
        {
            string result = Environment.NewLine;

            for (int row = 0; row < _height; row++)
            {
                string rowString = string.Empty;

                for (int column = 0; column < _width; column++)
                {
                    bool pixel = _display[column, row];
                    rowString += pixel ? "1" : " ";
                }

                result += rowString;
                result += Environment.NewLine;
            }

            result += Environment.NewLine;

            return result;
        }

        private void InitialiseDisplay(int width, int height)
        {
            _height = height;
            _width = width;
            _display = new bool[width, height];
        }

        private void ProcessInstructions()
        {
            const string numberPattern = @"\d+";

            foreach (string instruction in GetInput())
            {
                var numbers = Regex.Matches(instruction, numberPattern).Cast<Match>().Select(m => int.Parse(m.Value)).ToArray();

                if (instruction.Contains("rect"))
                {
                    SwitchOnRectangle(numbers[0], numbers[1]);
                }

                if (instruction.Contains("column"))
                {
                    RotateColumn(numbers[0], numbers[1]);
                }

                if (instruction.Contains("row"))
                {
                    RotateRow(numbers[0], numbers[1]);
                }
            }
        }

        private void SwitchOnRectangle(int width, int height)
        {
            for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                _display[x, y] = true;
            }
        }

        private void RotateRow(int row, int rotation)
        {
            foreach(var r in Enumerable.Range(1, rotation))
            {
                bool valueDroppingOffRow = _display[_width - 1, row];

                for (int column = _width - 1; column > 0; column--)
                {
                    _display[column, row] = _display[column - 1, row];
                }

                _display[0, row] = valueDroppingOffRow;
            }
        }

        private void RotateColumn(int column, int rotation)
        {
            foreach (var r in Enumerable.Range(1, rotation))
            {
                bool valueDroppingOffColumn = _display[column, _height - 1];

                for (int row = _height - 1; row > 0; row--)
                {
                    _display[column, row] = _display[column, row - 1];
                }

                _display[column, 0] = valueDroppingOffColumn;
            }
        }

    }
}
