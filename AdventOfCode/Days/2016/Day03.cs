﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2016
{
    public class Day03 : AdventDay, IAdventDay
    {
        public Day03() : base(2016, 3)
        {
        }

        public string CalculatePartA()
        {
            string intPattern = "\\d+";
                        
            var input = GetInput().Select(str => Regex.Matches(str, intPattern).Cast<Match>().Select(m => int.Parse(m.Value)).ToArray());

            return input.Count(IsValidTriangle).ToString();
        }

        public string CalculatePartB()
        {
            string intPattern = "\\d+";
            var input = GetInput().Select(str => Regex.Matches(str, intPattern).Cast<Match>().Select(m => int.Parse(m.Value)).ToArray())
                                  .ToList();

            List<int[]> triangles = new List<int[]>();
            int skip = 0;

            while (skip < input.Count())
            {
                var rows = input.Skip(skip).Take(3);

                triangles.Add(rows.Select(arr => arr[0]).ToArray());
                triangles.Add(rows.Select(arr => arr[1]).ToArray());
                triangles.Add(rows.Select(arr => arr[2]).ToArray());

                skip += 3;
            }

            return triangles.Count(IsValidTriangle).ToString();
        }

        private bool IsValidTriangle(int[] sides)
        {
            bool check1 = sides[0] + sides[1] > sides[2];
            bool check2 = sides[0] + sides[2] > sides[1];
            bool check3 = sides[1] + sides[2] > sides[0];

            return check1 && check2 && check3;
        }
    }
}
