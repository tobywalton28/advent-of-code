﻿using System;
using System.Linq;
using System.Text;

namespace AdventOfCode.Days._2016
{
    public class Day05 : AdventDay, IAdventDay
    {
        public Day05() : base(2016, 5) { }

        public string CalculatePartA()
        {
            Func<string, string, string> passwordUpdate = (password, hash) =>
            {
                char ch = hash.ToCharArray()[5];
                var passwordChars = password.ToCharArray();
                var index = Array.IndexOf(passwordChars, '*');
                passwordChars[index] = ch;

                return new string(passwordChars);
            };

            return CalculatePassword("ffykfhsq", passwordUpdate);
        }

        public string CalculatePartB()
        {
            Func<string, string, string> passwordUpdate = (password, hash) =>
            {
                int index;
                if (!int.TryParse(hash.ToCharArray()[5].ToString(), out index))
                {
                    return password;
                }

                char ch = hash.ToCharArray()[6];
                var passwordChars = password.ToCharArray();
                
                if (index < passwordChars.Length && passwordChars[index] == '*')
                {
                    passwordChars[index] = ch;
                }
                
                return new string(passwordChars);
            };

            return CalculatePassword("ffykfhsq", passwordUpdate);
        }

        private string CalculatePassword(string doorId, Func<string, string, string> passwordUpdate)
        {
            int index = 0;
            string password = "********";

            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            while (password.ToCharArray().Any(ch => ch == '*'))
            {
                string valueToHash = doorId + index.ToString();
                string hash = CreateMD5(valueToHash, md5);
                if (hash.StartsWith("00000"))
                {
                    password = passwordUpdate(password, hash);
                }

                index += 1;
            }

            return password.ToLower();
        }

        private string CreateMD5(string input, System.Security.Cryptography.MD5 md5)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
