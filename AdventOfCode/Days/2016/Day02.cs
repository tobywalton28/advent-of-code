﻿namespace AdventOfCode.Days._2016
{
    public class Day02 : AdventDay, IAdventDay
    {
        private int _rowIndex = 1;
        private int _colIndex = 1;
        private string _result;
        private string[,] _buttons;

        public Day02() : base(2016, 2)
        {
        }

        public string CalculatePartA()
        {
            string[,] buttons = {
                { "1", "2", "3" },
                { "4", "5", "6" },
                { "7", "8", "9" }
            };

            ProcessInstructions(buttons, 1, 1);

            return _result;
        }

        public string CalculatePartB()
        {
            string[,] buttons = {
                { null, null, "1", null, null },
                { null, "2",  "3", "4",  null },
                { "5",  "6",  "7", "8",  "9"  },
                { null, "A",  "B", "C",  null },
                { null, null, "D", null, null }
            };

            ProcessInstructions(buttons, 2, 0);

            return _result;
        }

        private void ProcessInstructions(string[,] buttons, int rowIndex, int colIndex)
        {
            _result = string.Empty;
            _buttons = buttons;
            _rowIndex = rowIndex;
            _colIndex = colIndex;

            foreach (var instruction in GetInput())
            {
                ProcessInstruction(instruction);
            }
        }

        private void ProcessInstruction(string instruction)
        {            
            foreach (var direction in instruction.ToCharArray())
            {
                int newRowIndex = _rowIndex;
                int newColIndex = _colIndex;

                switch (direction)
                {
                    case 'U':
                        newRowIndex--;
                        break;
                    case 'D':
                        newRowIndex++;
                        break;
                    case 'L':
                        newColIndex--;
                        break;
                    case 'R':
                        newColIndex++;
                        break;
                }

                if (LocationValid(newRowIndex, newColIndex))
                {
                    _rowIndex = newRowIndex;
                    _colIndex = newColIndex;
                }
            }

            var button = _buttons[_rowIndex, _colIndex];
            _result += button;
        }

        private bool LocationValid(int rowIndex, int colIndex)
        {
            int maxIndex = _buttons.GetUpperBound(0);

            return rowIndex >= 0 && rowIndex <= maxIndex && colIndex >= 0 && colIndex <= maxIndex && _buttons[rowIndex, colIndex] != null;
        }

    }
}
