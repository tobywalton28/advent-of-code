﻿using AdventOfCode.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    [Awesome]
    public class Day07 : AdventDay, IAdventDay
    {
        private readonly string[] Gates = new[] { "AND", "OR", "LSHIFT", "RSHIFT", "NOT" };
        private const string NumberPattern = "\\d+";
        private const string WireNamePattern = "[a-z]+";
        private Dictionary<string, int> _wires;
        private Instruction[] _instructions;

        public Day07() : base(2015, 7)
        {
        }

        public string CalculatePartA()
        {
            _instructions = GetInstructions();
            _wires = GetWires(_instructions);
            var voltageInstructions = GetVoltageInstructions(_instructions);

            foreach (var i in voltageInstructions)
            {
                int voltage = int.Parse(i.Operation);
                ApplyVoltage(voltage, i.OutputWire);
            }

            return _wires["a"].ToString();
        }

        public string CalculatePartB()
        {
            _instructions = GetInstructions();
            _wires = GetWires(_instructions);
            var voltageInstructions = GetVoltageInstructions(_instructions);

            foreach (var i in voltageInstructions)
            {
                int voltage = int.Parse(i.Operation);
                ApplyVoltage(voltage, i.OutputWire);
            }

            ApplyVoltage(_wires["a"], "b");

            return _wires["a"].ToString();
        }

        Dictionary<string, int> GetWires(Instruction[] instructions)
        {
            return _instructions.SelectMany(i => Regex.Matches(i.Raw, WireNamePattern).Cast<Match>().Select(m => m.Value))
                                .Distinct()
                                .ToDictionary(name => name, name => 0);
        }

        Instruction[] GetVoltageInstructions(Instruction[] instructions)
        {
            int parsedValue;

            return _instructions.Where(i => int.TryParse(i.Operation, out parsedValue))
                                .ToArray();
        }

        private void ApplyVoltage(int voltage, string wireName)
        {
            _wires[wireName] = voltage;

            var inputWires = new string[] { wireName };
            var count = 0;

            while (inputWires.Any())
            {
                inputWires = TraceWires(inputWires);
                count++;
            }
        }

        private string[] TraceWires(params string[] wires)
        {
            var result = _instructions.Where(i => i.InputWires.Any(w => wires.Contains(w)));

            foreach (var instruction in result)
            {
                ProcessInstruction(instruction);
            }

            return result.Select(w => w.OutputWire).ToArray();
        }

        private void ProcessInstruction(Instruction i)
        {
            string gateName = "WIRE TO WIRE";
            var gate = Gates.SingleOrDefault(g => i.Operation.Contains(g));
            if (gate != null) gateName = gate;
            int shift;

            switch (gateName)
            {
                case "AND":
                    if (i.InputWires.Length == 2)
                    {
                        _wires[i.OutputWire] = _wires[i.InputWires[0]] & _wires[i.InputWires[1]];
                    }
                    else if (i.InputWires.Length == 1)
                    {
                        int fixedSignal = int.Parse(Regex.Match(i.Operation, NumberPattern).Value);
                        _wires[i.OutputWire] = _wires[i.InputWires[0]] & fixedSignal;
                    }
                    else
                    {
                        throw new Exception("inputs missing");
                    }
                    break;
                case "OR":
                    if (i.InputWires.Length != 2) throw new Exception("need two inputs");
                    _wires[i.OutputWire] = _wires[i.InputWires[0]] | _wires[i.InputWires[1]];
                    break;
                case "LSHIFT":
                    shift = int.Parse(Regex.Match(i.Operation, NumberPattern).Value);
                    if (i.InputWires.Length != 1) throw new Exception("need one inputs");
                    _wires[i.OutputWire] = _wires[i.InputWires[0]] << shift;
                    break;
                case "RSHIFT":
                    shift = int.Parse(Regex.Match(i.Operation, NumberPattern).Value);
                    if (i.InputWires.Length != 1) throw new Exception("need one inputs");
                    _wires[i.OutputWire] = _wires[i.InputWires[0]] >> shift;
                    break;
                case "NOT":
                    if (i.InputWires.Length != 1) throw new Exception("need one inputs");
                    _wires[i.OutputWire] = Not(_wires[i.InputWires[0]]); // TODO - make shift dynamic
                    break;
                case "WIRE TO WIRE":
                    _wires[i.OutputWire] = _wires[i.InputWires[0]];
                    break;
                default:
                    throw new Exception($"gate {gateName} not implemented");
            }
        }

        private int Not(int input)
        {
            string binaryString = Convert.ToString(input, 2).PadLeft(16, '0');
            StringBuilder result = new StringBuilder();

            foreach (char ch in binaryString)
            {
                result.Append(ch == '0' ? "1" : "0");
            }

            return Convert.ToInt32(result.ToString(), 2);
        }

        private Instruction[] GetInstructions()
        {
            string[] input = GetInput();

            return input.Select(str =>
            {
                string operation = str.Split(new[] { "->" }, StringSplitOptions.None)[0].Trim();
                string[] inputWires = Regex.Matches(operation, WireNamePattern).Cast<Match>().Select(m => m.Value).ToArray();
                string outputWire = str.Split(new[] { "->" }, StringSplitOptions.None)[1].Trim();

                return new Instruction(inputWires, outputWire, operation, str);
            }).ToArray();
        }

        private class Instruction
        {
            public Instruction(string[] inputWires, string outputWire, string operation, string raw)
            {
                InputWires = inputWires;
                OutputWire = outputWire;
                Operation = operation;
                Raw = raw;
            }

            public string[] InputWires { get; set; }

            public string OutputWire { get; }

            public string Operation { get; }

            public string Raw { get; }
        }
    }
}
