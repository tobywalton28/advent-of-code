﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day02 : AdventDay, IAdventDay
    {
        public Day02() : base(2015, 2)
        {
        }

        public string CalculatePartA()
        {
            return GetAllPresents().Select(present => present.WrappingPaperRequired()).Sum().ToString();            
        }

        public string CalculatePartB()
        {
            return GetAllPresents().Select(present => present.RibbonLength()).Sum().ToString();
        }

        private Present[] GetAllPresents()
        {
            var presents = GetInput()
                .Select(str => str.Split('x'))
                .Select(arr => new Present(int.Parse(arr[0]), int.Parse(arr[1]), int.Parse(arr[2])));

            return presents.ToArray();
        }

        private class Present
        {
            public Present(int length, int width, int height)
            {
                Length = length;
                Width = width;
                Height = height;
            }

            private int Length { get; }

            private int Width { get; }

            private int Height { get; }

            public int WrappingPaperRequired()
            {
                int area1 = 2 * Length * Width;
                int area2 = 2 * Width * Height;
                int area3 = 2 * Height * Length;
                int smallestSideArea = Math.Min(Math.Min(Length * Width, Width * Height), Height * Length);


                return area1 + area2 + area3 + smallestSideArea;
            }

            public int Volume()
            {
                return Length * Height * Width;
            }

            public int RibbonLength()
            {
                IEnumerable<int> smallestSides = new List<int> { Length, Width, Height }.OrderBy(i => i).Take(2);

                return 2 * smallestSides.Sum() + Volume();
            }

        }     
    }
}
