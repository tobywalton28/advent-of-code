﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day22 : AdventDay, IAdventDay
    {
        public Day22() : base(2015, 22)
        {
        }

        public string CalculatePartA()
        { 
            var combos = GetAttackCombinations(10);
            int minimumWinningMana = Int32.MaxValue;

            foreach (var combo in combos)
            {
                var player = new Player(50, 500);
                var boss = new Boss(55, 8);
                var result = new Fight().HaveFight(combo, player, boss, false);
                if (result.Outcome == FightOutcome.PlayerWins && result.ManaSpent < minimumWinningMana)
                {
                    minimumWinningMana = result.ManaSpent;
                }
            }

            return minimumWinningMana.ToString();            
        }

        public string CalculatePartB()
        {
            var combos = GetAttackCombinations(10);
            int minimumWinningMana = Int32.MaxValue;

            foreach (var combo in combos)
            {
                var player = new Player(50, 500);
                var boss = new Boss(55, 8);
                var result = new Fight().HaveFight(combo, player, boss, true);
                if (result.Outcome == FightOutcome.PlayerWins && result.ManaSpent < minimumWinningMana)
                {
                    minimumWinningMana = result.ManaSpent;
                }
            }

            return minimumWinningMana.ToString();
        }

        private class Fight
        {
            private Dictionary<AttackType, int> _effects = new Dictionary<AttackType, int>();
            private Player _player;
            private Boss _boss;
            private int _manaSpent = 0;
            private FightOutcome? _fightOutcome;
            private bool _playerInjured;

            public FightResult HaveFight(AttackType[] attacks, Player player, Boss boss, bool playerInjured)
            {                
                _player = player;
                _boss = boss;
                _playerInjured = playerInjured;

                foreach (var attack in attacks)
                {
                    FightOutcome? outcome = TakePlayerTurn(attack);

                    if (outcome != null)
                    {
                        return new FightResult { Outcome = outcome.Value, ManaSpent = _manaSpent };
                    }

                    outcome = TakeBossTurn();

                    if (outcome != null)
                    {
                        return new FightResult { Outcome = outcome.Value, ManaSpent = _manaSpent };
                    }
                }

                return new FightResult { Outcome = FightOutcome.NotFinished };
            }

            private void SpendMana(int mana)
            {
                _player.Mana -= mana;
                _manaSpent += mana;
            }

            private FightOutcome? TakePlayerTurn(AttackType attackType)
            {
                if (_playerInjured) _player.HitPoints -= 1;
                ExecuteEffects();
                if (_boss.Defeated) return FightOutcome.PlayerWins;                

                switch (attackType)
                {
                    case AttackType.MagicMissile:
                        SpendMana(53);
                        _boss.HitPoints -= 4;
                        break;
                    case AttackType.Drain:
                        SpendMana(73);
                        _boss.HitPoints -= 2;
                        _player.HitPoints += 2;
                        break;
                    case AttackType.Shield:
                        SpendMana(113);
                        if (_effects.ContainsKey(AttackType.Shield)) return FightOutcome.InvalidAttack;
                        _effects.Add(AttackType.Shield, 6);
                        break;
                    case AttackType.Poison:
                        SpendMana(173);
                        if (_effects.ContainsKey(AttackType.Poison)) return FightOutcome.InvalidAttack;
                        _effects.Add(AttackType.Poison, 6);
                        break;
                    case AttackType.Recharge:
                        SpendMana(229);
                        if (_effects.ContainsKey(AttackType.Recharge)) return FightOutcome.InvalidAttack;
                        _effects.Add(AttackType.Recharge, 5);
                        break;
                }

                if (_player.Mana < 0 || _player.HitPoints <= 0) return FightOutcome.BossWins;

                if (_boss.Defeated) return FightOutcome.PlayerWins;

                return null;
            }

            private FightOutcome? TakeBossTurn()
            {
                ExecuteEffects();
                if (_boss.Defeated) return FightOutcome.PlayerWins;

                int damage = Math.Max(1, _boss.Damage - _player.Armor);
                _player.HitPoints -= damage;

                if (_player.Defeated) return FightOutcome.BossWins;

                return null;
            }

            private void ExecuteEffects()
            {
                foreach (var effect in _effects.Keys)
                {
                    switch (effect)
                    {
                        case AttackType.Poison:
                            _boss.HitPoints -= 3;
                            break;
                        case AttackType.Recharge:
                            _player.Mana += 101;
                            break;
                    }                    
                }

                _effects = _effects.Where(e => e.Value > 1).ToDictionary(e => e.Key, e => e.Value - 1);
                _player.Armor = _effects.ContainsKey(AttackType.Shield) ? 7 : 0;

            }
        }

        private IEnumerable<AttackType[]> GetAttackCombinations(int attackCount)
        {
            var attackTypes = new AttackType[] {AttackType.MagicMissile, AttackType.Drain, AttackType.Shield, AttackType.Poison, AttackType.Recharge};

            var result = new List<List<AttackType>>
            {
                new List<AttackType>{ AttackType.MagicMissile },
                new List<AttackType>{ AttackType.Drain },
                new List<AttackType>{ AttackType.Shield },
                new List<AttackType>{ AttackType.Poison },
                new List<AttackType>{ AttackType.Recharge }
            };
            
            for (int attack = 2; attack <= attackCount; attack++)
            {
                var newResult = new List<List<AttackType>>();

                foreach (var attackCombination in result)
                foreach (var attackType in attackTypes)
                {
                    var newCombination = attackCombination.Select(at => at).ToList();
                    newCombination.Add(attackType);
                    newResult.Add(newCombination);
                }

                result = newResult;
            }

            return result.Select(attacks => attacks.ToArray());
        }

        private class Player
        {
            public Player(int hitPoints, int mana)
            {
                HitPoints = hitPoints;
                Mana = mana;
            }

            public bool Defeated => HitPoints <= 0 || Mana < 0;

            public int HitPoints { get; set; }

            public int Mana { get; set; }

            public int Armor { get; set; }
        }

        private class Boss
        {
            public Boss(int hitPoints, int damage)
            {
                HitPoints = hitPoints;
                Damage = damage;
            }

            public bool Defeated => HitPoints <= 0;

            public int HitPoints { get; set; }

            public int Damage { get; private set; }
        }

        private enum AttackType
        {
            MagicMissile,
            Drain,
            Shield,
            Poison,
            Recharge
        }

        private class FightResult
        {
            public FightOutcome Outcome { get; set; }

            public int ManaSpent { get; set; }
        }

        private enum FightOutcome
        {
            PlayerWins,
            BossWins,
            InvalidAttack,
            NotFinished
        }
    }
}
