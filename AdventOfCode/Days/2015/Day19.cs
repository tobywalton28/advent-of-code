﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day19 : AdventDay, IAdventDay
    {
        public Day19() : base(2015, 19)
        {
        }

        public string CalculatePartA()
        {
            var instructions = GetInstructions();
            var molecules = instructions.Replacements.SelectMany(rep => GetMolecules(instructions.Input, rep));

            return molecules.Distinct().Count().ToString();
        }

        public string CalculatePartB()
        {
            var instructions = GetInstructions();
            var result = instructions.Input;
            int replacementsMade = 0;
            var replacement = GetMostLucrativeReplacement(instructions.Input, instructions.Replacements);

            while (replacement != null)
            {
                result = MakeReplacement(result, replacement);
                replacementsMade++;
                replacement = GetMostLucrativeReplacement(result, instructions.Replacements);
            }
            
            if (result != "e") throw new Exception("something went wrong!");

            return replacementsMade.ToString();
        }

        private string[] GetMolecules(string input, Replacement replacement)
        {
            var matches = Regex.Matches(input, replacement.OriginalText, RegexOptions.None).Cast<Match>();

            return matches.Select(m => input.Substring(0, m.Index) + replacement.ReplacementText + input.Substring(m.Index + m.Length))
                          .ToArray();
        }

        private string MakeReplacement(string molecule, Replacement replacement)
        {
            var match = Regex.Match(molecule, replacement.ReplacementText);

            string part1 = molecule.Substring(0, match.Index);
            string part2 = molecule.Substring(match.Index + match.Length);

            return part1 + replacement.OriginalText + part2;
        }

        private Replacement GetMostLucrativeReplacement(string molecule, Replacement[] replacements)
        {
            return replacements.Where(r => molecule.Contains(r.ReplacementText))
                               .OrderByDescending(r => r.Reduction)
                               .FirstOrDefault();            
        }

        private Instructions GetInstructions()
        {
            var input = GetInput();

            var replacements = input.Where(str => str.Contains("=>"))
                                    .Select(str => {
                                        var replacement = str.Split(new[] { "=>" }, StringSplitOptions.None)
                                                             .Select(s => s.Trim())
                                                             .ToArray();

                                        return new Replacement(replacement[0], replacement[1]);
                                    }).ToArray();

            return new Instructions(input.Last(), replacements);
        }

        private class Instructions
        {
            public Instructions(string input, Replacement[] replacements)
            {
                Input = input;
                Replacements = replacements;
            }

            public string Input { get; }

            public Replacement[] Replacements { get; }
        }

        private class Replacement
        {
            public Replacement(string originalTest, string replacementText)
            {
                OriginalText = originalTest;
                ReplacementText = replacementText;
            }

            public string OriginalText { get; }

            public string ReplacementText { get; }

            public int Reduction => ReplacementText.Length - OriginalText.Length;
        }
    }
}
