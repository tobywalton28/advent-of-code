﻿using System.Collections.Generic;

namespace AdventOfCode.Days._2015
{
    public class Day03 : AdventDay, IAdventDay
    {
        public Day03() : base(2015, 3)
        {
        }

        private int _north = 0;
        private int _east = 0;
        private readonly Dictionary<string, int> _deliveries = new Dictionary<string, int>();

        public string CalculatePartA()
        {
            char[] input = GetInput()[0].ToCharArray();

            var santa = new Santa();
            DeliverPresent(santa);

            foreach (char instruction in input)
            {
                santa.Move(instruction);
                DeliverPresent(santa);
            }

            return _deliveries.Count.ToString();
        }

        public string CalculatePartB()
        {
            char[] input = GetInput()[0].ToCharArray();
            var realSanta = new Santa();
            var roboSanta = new Santa();

            var currentSanta = realSanta;

            DeliverPresent(currentSanta);

            foreach (char instruction in input)
            {
                currentSanta.Move(instruction);
                DeliverPresent(currentSanta);
                currentSanta = ReferenceEquals(currentSanta, realSanta) ? roboSanta : realSanta;
            }

            return _deliveries.Count.ToString();
        }

        private void DeliverPresent(Santa santa)
        {
            if (!_deliveries.ContainsKey(santa.Location())) _deliveries.Add(santa.Location(), 0);
            _deliveries[santa.Location()] += 1;
        }

        private class Santa
        {
            private int _north = 0;
            private int _east = 0;

            public string Location()
            {
                return $"{_north},{_east}";
            }

            public void Move(char direction)
            {
                if (direction == '^') _north += 1;
                if (direction == 'v') _north -= 1;
                if (direction == '>') _east += 1;
                if (direction == '<') _east -= 1;
            }
        }
    }
}
