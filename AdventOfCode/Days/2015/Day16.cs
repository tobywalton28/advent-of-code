﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day16 : AdventDay, IAdventDay
    {
        public Day16() : base(2015, 16)
        {
        }

        public string CalculatePartA()
        {
            var sues = GetSues();

            var theRightSue = sues.Single(s => 
                (s.Children == 3 || s.Children == null)
                &&
                (s.Cats == 7 || s.Cats == null)
                &&
                (s.Samoyeds == 2 || s.Samoyeds == null)
                &&
                (s.Pomeranians == 3 || s.Pomeranians == null)
                &&
                (s.Akitas == 0 || s.Akitas == null)
                &&
                (s.Vizslas == 0 || s.Vizslas == null)
                &&
                (s.Goldfish == 5 || s.Goldfish == null)
                &&
                (s.Trees == 3 || s.Trees == null)
                &&
                (s.Cars == 2 || s.Cars == null)
                &&
                (s.Perfumes == 1 || s.Perfumes == null)
                );            

            return theRightSue.Name;
        }

        public string CalculatePartB()
        {
            var sues = GetSues();

            var theRightSue = sues.Single(s =>
                (s.Children == 3 || s.Children == null)
                &&
                (s.Cats > 7 || s.Cats == null)
                &&
                (s.Samoyeds == 2 || s.Samoyeds == null)
                &&
                (s.Pomeranians < 3 || s.Pomeranians == null)
                &&
                (s.Akitas == 0 || s.Akitas == null)
                &&
                (s.Vizslas == 0 || s.Vizslas == null)
                &&
                (s.Goldfish < 5 || s.Goldfish == null)
                &&
                (s.Trees > 3 || s.Trees == null)
                &&
                (s.Cars == 2 || s.Cars == null)
                &&
                (s.Perfumes == 1 || s.Perfumes == null)
            );

            return theRightSue.Name;
        }

        private Sue[] GetSues()
        {
            return GetInput().Select(str =>
                {
                    return new Sue
                    (
                        str.Substring(0, str.IndexOf(':')), 
                        GetCompound(str, "children"),
                        GetCompound(str, "cats"), 
                        GetCompound(str, "samoyeds"), 
                        GetCompound(str, "pomeranians"), 
                        GetCompound(str, "akitas"), 
                        GetCompound(str, "vizslas"), 
                        GetCompound(str, "goldfish"), 
                        GetCompound(str, "trees"), 
                        GetCompound(str, "cars"), 
                        GetCompound(str, "perfumes")
                    );
                }).ToArray();
        }

        private int? GetCompound(string sueString, string compound)
        {
            string pattern = $"{compound}: \\d+";
            var match = Regex.Match(sueString, pattern);

            return match.Success ? int.Parse(Regex.Match(match.Value, "\\d+").Value) : (int?)null;
        }

        private class Sue
        {
            public Sue(
                string name, 
                int? children, 
                int? cats, 
                int? samoyeds, 
                int? pomeranians, 
                int? akitas, 
                int? vizslas, 
                int? goldfish, 
                int? trees, 
                int? cars, 
                int? perfumes)
            {
                Name = name;
                Children = children;
                Cats = cats;
                Samoyeds = samoyeds;
                Pomeranians = pomeranians;
                Akitas = akitas;
                Vizslas = vizslas;
                Goldfish = goldfish;
                Trees = trees;
                Cars = cars;
                Perfumes = perfumes;
            }

            public string Name { get; }

            public int? Children { get; }

            public int? Cats { get; }

            public int? Samoyeds { get; }

            public int? Pomeranians { get; }

            public int? Akitas { get; }

            public int? Vizslas { get; }

            public int? Goldfish { get; }

            public int? Trees { get; }

            public int? Cars { get; }

            public int? Perfumes { get; }
        }
    }
}
