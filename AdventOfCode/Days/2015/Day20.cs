﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day20 : AdventDay, IAdventDay
    {
        private const int PRESENTS_REQUIRED = 29000000;
        private const int MAX_HOUSE_NUMBER = 750000;

        private Dictionary<int, int> _deliveries;

        public Day20() : base(2015, 20)
        {
        }

        public string CalculatePartA()
        {
            MakeDeliveries(10, null);

            return GetLowestQualifyingHouseNumber().ToString();
        }

        public string CalculatePartB()
        {
            MakeDeliveries(11, 50);

            return GetLowestQualifyingHouseNumber().ToString();
        }

        private int GetLowestQualifyingHouseNumber()
        {
            return _deliveries.Where(d => d.Value >= PRESENTS_REQUIRED)
                  .OrderBy(d => d.Key)
                  .First()
                  .Key;
        }

        private void MakeDeliveries(int presentMultiplier, int? deliveriesPerElf)
        {
            _deliveries = new Dictionary<int, int>();

            for (int elf = 1; elf <= MAX_HOUSE_NUMBER; elf++)
            {
                int houseNumber = elf;
                int deliveriesMade = 0;

                while (houseNumber <= MAX_HOUSE_NUMBER && (deliveriesPerElf == null || deliveriesMade <= deliveriesPerElf.Value))
                {
                    if (!_deliveries.ContainsKey(houseNumber)) _deliveries.Add(houseNumber, 0);
                    _deliveries[houseNumber] += elf * presentMultiplier;
                    houseNumber += elf;
                    deliveriesMade += 1;
                }
            }
        }
    }
}
