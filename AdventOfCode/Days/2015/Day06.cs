﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day06 : AdventDay, IAdventDay
    {
        private Light[] _allLights;
        private Instruction[] _allInstructions;

        public Day06() : base(2015, 6)
        {
        }

        public string CalculatePartA()
        {
            string[] input = GetInput();
            _allInstructions = GetInstructions(input);
            _allLights = CreateLights();

            ProcessInstructions();

            return _allLights.Count(light => light.Status == LightStatus.On).ToString();
        }

        public string CalculatePartB()
        {
            string[] input = GetInput();
            _allInstructions = GetInstructions(input);
            _allLights = CreateLights();

            ProcessInstructions();

            return _allLights.Sum(light => light.Brightness).ToString();
        }

        Light[] CreateLights()
        {
            int[] integers = Enumerable.Range(0, 1000).ToArray();

            return integers.SelectMany(x => integers, (x, y) => new Light(new Position(x, y), LightStatus.Off))
                           .ToArray();
        }   

        private void ProcessInstructions()
        {
            foreach (Instruction instruction in _allInstructions)
            {
                var matches = _allLights.Where(light => light.InstructionRelevant(instruction));
                foreach (Light light in matches) light.ApplyInstruction(instruction);
            }
        }

        private Instruction[] GetInstructions(string[] input)
        {
            string pattern = $"\\d+,\\d+";

            return input.Select(str =>
            {
                MatchCollection coordinateStrings = Regex.Matches(str, pattern);
                Position positionA = new Position(int.Parse(coordinateStrings[0].Value.Split(',')[0]), int.Parse(coordinateStrings[0].Value.Split(',')[1]));
                Position positionB = new Position(int.Parse(coordinateStrings[1].Value.Split(',')[0]), int.Parse(coordinateStrings[1].Value.Split(',')[1]));

                return new Instruction(GetOperation(str), positionA, positionB);
            }).ToArray();
        }

        private Operation GetOperation(string instruction)
        {
            if (instruction.Contains("toggle")) return Operation.Toggle;
            if (instruction.Contains("turn on")) return Operation.On;
            if (instruction.Contains("turn off")) return Operation.Off;

            throw new Exception("unknown operation");
        }

        private class Light
        {
            public Light(Position position, LightStatus status)
            {
                Position = position;
                Status = status;
            }

            public Position Position { get; }

            public LightStatus Status { get; private set; }

            public int Brightness { get; private set; }

            public bool InstructionRelevant(Instruction instruction)
            {
                bool xMatch = Position.X >= instruction.PositionA.X && Position.X <= instruction.PositionB.X;
                bool yMatch = Position.Y >= instruction.PositionA.Y && Position.Y <= instruction.PositionB.Y;

                return xMatch && yMatch;
            }

            public void ApplyInstruction(Instruction instruction)
            {
                switch (instruction.Operation)
                {
                    case Operation.On:
                        Status = LightStatus.On;
                        Brightness += 1;
                        break;
                    case Operation.Off:
                        Status = LightStatus.Off;
                        Brightness = Math.Max(0, Brightness - 1);
                        break;
                    case Operation.Toggle:
                        Status = Status == LightStatus.On ? LightStatus.Off : LightStatus.On;
                        Brightness += 2;
                        break;
                }
            }
        }

        private class Instruction
        {
            public Instruction(Operation operation, Position positionA, Position positionB)
            {
                Operation = operation;
                PositionA = positionA;
                PositionB = positionB;
            }

            public Operation Operation { get; }

            public Position PositionA { get; }

            public Position PositionB { get; }
        }

        private class Position
        {
            public Position(int x, int y)
            {
                X = x;
                Y = y;
            }

            public int X { get; }

            public int Y { get; }
        }

        private enum Operation
        {
            On,
            Off,
            Toggle
        }

        private enum LightStatus
        {
            Off,
            On
        }
    }
}
