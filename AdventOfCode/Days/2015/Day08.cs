﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day08 : AdventDay, IAdventDay
    { 
        public Day08() : base(2015, 8)
        {
        }

        public string CalculatePartA()
        {
            var input = GetInput();

            int totalCode = input.Sum(l => l.Length);
            int totalCharacters = input.Sum(CharacterCount);                        
            
            return (totalCode - totalCharacters).ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput();

            int totalCode = input.Sum(l => l.Length);            
            int totalEncoded = input.Sum(EncodedStringCount);
            
            return (totalEncoded - totalCode).ToString();
        }

        int CharacterCount(string arg) => Regex.Match(arg, @"^""(\\x..|\\.|.)*""$").Groups[1].Captures.Count;
        int EncodedStringCount(string arg) => 2 + arg.Sum(CharsToEncode);
        int CharsToEncode(char c) => c == '\\' || c == '\"' ? 2 : 1;
    }
}
