﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode.Days._2015
{
    public class Day04 : AdventDay, IAdventDay
    {
        public Day04() : base(2015, 4)
        {
        }

        public string CalculatePartA()
        {
            return FindNumber("ckczppom", hashResult => hashResult.StartsWith("00000")).ToString();
        }

        public string CalculatePartB()
        {
            return FindNumber("ckczppom", hashResult => hashResult.StartsWith("000000")).ToString();
        }

        private int FindNumber(string input, Func<string, bool> isComplete)
        {
            int number = 0;
            string hashResult = string.Empty;

            using (var md5 = MD5.Create())
            while (!isComplete(hashResult))
            {
                number += 1;
                string hash = $"{input}{number}";
                hashResult = ToMd5Hash(hash, md5);
            }

            return number;
        }

        private string ToMd5Hash(string str, MD5 md5)
        {
            var bytes = Encoding.ASCII.GetBytes(str);

            if (bytes.Length == 0) return null;

            return string.Join("", md5.ComputeHash(bytes).Select(x => x.ToString("X2")));
        }
    }
}
