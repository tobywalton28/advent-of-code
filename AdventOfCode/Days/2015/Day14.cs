﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day14 : AdventDay, IAdventDay
    {
        public Day14() : base(2015, 14)
        {
        }

        public string CalculatePartA()
        {
            var instructions = GetInstructions().ToList();

            var distances = instructions.Select(i => CalculateDistanceAfterSeconds(i, 2503)).ToList();

            return distances.Max().ToString();
        }

        public string CalculatePartB()
        {
            var instructions = GetInstructions().ToList();
            var points = instructions.ToDictionary(i => i.Reindeer, i => 0);

            for (int second = 1; second <= 2503; second++)
            {
                var distances = instructions.Select(i => new{ Reindeer = i.Reindeer, Distance = CalculateDistanceAfterSeconds(i, second) }).ToList();
                int maxDistance = distances.Max(d => d.Distance);
                var winningReindeer = distances.Where(d => d.Distance == maxDistance);

                foreach (var reindeer in winningReindeer)
                {
                    points[reindeer.Reindeer] += 1;
                }
            }

            return points.Max(p => p.Value).ToString();
        }

        public int CalculateDistanceAfterSeconds(Instruction instruction, int seconds)
        {
            int cycleSeconds = instruction.FlyingSeconds + instruction.RestingSeconds;
            int completedCycles = int.Parse(Math.Floor((double)(seconds / cycleSeconds)).ToString());            
            int partialSeconds = seconds - (cycleSeconds * completedCycles);
            int flyingSeconds = Math.Min(partialSeconds, instruction.FlyingSeconds);
            int totalSecondsFlying = (instruction.FlyingSeconds * completedCycles) + flyingSeconds;

            return totalSecondsFlying * instruction.Speed;
        }

        private IEnumerable<Instruction> GetInstructions()
        {
            return GetInput().Select(str =>
            {                
                string reindeer = str.Substring(0, str.IndexOf(" ", StringComparison.Ordinal)).Trim();
                var numbers = Regex.Matches(str, $"\\d+");

                return new Instruction(reindeer, int.Parse(numbers[0].Value), int.Parse(numbers[1].Value), int.Parse(numbers[2].Value));
            });
        }

        public class Instruction
        {
            public Instruction(string reindeer, int speed, int flyingSeconds, int restingSeconds)
            {
                Reindeer = reindeer;
                Speed = speed;
                FlyingSeconds = flyingSeconds;
                RestingSeconds = restingSeconds;
            }

            public string Reindeer { get; }

            public int Speed { get; }

            public int FlyingSeconds { get; }

            public int RestingSeconds { get; }
        }
    }
}
