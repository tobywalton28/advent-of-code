﻿using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day18 : AdventDay, IAdventDay
    {
        public Day18() : base(2015, 18)
        {
        }

        public string CalculatePartA()
        {
            var lights = GetInitialState();

            for (int step = 1; step <= 100; step++)
            {
                lights = Animate(lights);
            }

            return CountSwitchedOn(lights).ToString();
        }

        public string CalculatePartB()
        {
            var lights = GetInitialState();
            SwitchOnCorners(lights);

            for (int step = 1; step <= 100; step++)
            {
                lights = Animate(lights);
                SwitchOnCorners(lights);
            }

            return CountSwitchedOn(lights).ToString();
        }

        private void SwitchOnCorners(bool[,] lights)
        {
            lights[0, 0] = true;
            lights[0, 99] = true;
            lights[99, 0] = true;
            lights[99, 99] = true;
        }

        private bool[,] Animate(bool[,] lights)
        {
            var result = new bool[100,100];

            for (int row = 0; row <= 99; row++)
            {
                for (int col = 0; col <= 99; col++)
                {
                    var currentState = lights[row, col];
                    var switchedOnNeighbours = GetSwitchedOnNeighbours(row, col, lights);
                    result[row, col] = GetNextState(currentState, switchedOnNeighbours);
                }
            }

            return result;
        }

        private int CountSwitchedOn(bool[,] lights)
        {
            var rows = Enumerable.Range(0, 100);
            var cols = Enumerable.Range(0, 100);

            var allLights = rows.SelectMany(r => { return cols.Select(c => new { Row = r, Col = c }); }).ToList();

            return allLights.Select(n => lights[n.Row, n.Col]).Count(n => n);
        }

        private bool GetNextState(bool switchedOn, int switchedOnNeighbours)
        {
            if (switchedOn)
            {
                return switchedOnNeighbours == 2 || switchedOnNeighbours == 3;
            }

            return switchedOnNeighbours == 3;
        }

        private int GetSwitchedOnNeighbours(int row, int col, bool[,] lights)
        {            
            var rows = Enumerable.Range(row - 1, 3);
            var cols = Enumerable.Range(col - 1, 3);

            var neighbours = rows.SelectMany(r => { return cols.Select(c => new { Row = r, Col = c }); })
                                 .Where(c =>
                                            (c.Row >= 0 && c.Row <= 99)
                                            &&
                                            (c.Col >= 0 && c.Col <= 99)
                                            &&
                                            (c.Row != row || c.Col != col)
                                        ).ToList();

            return neighbours.Select(n => lights[n.Row, n.Col]).Count(n => n);            
        }

        private bool[,] GetInitialState()
        {
            var lights = new bool[100, 100];

            var input = GetInput();

            int row = 0;

            foreach (var str in input)
            {
                int col = 0;

                foreach (char ch in str.ToArray())
                {
                    lights[row, col] = ch == '#';
                    col++;
                }

                row++;
            }

            return lights;
        }
    }
}
