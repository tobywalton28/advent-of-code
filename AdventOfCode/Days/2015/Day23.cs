﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day23 : AdventDay, IAdventDay
    {
        private Dictionary<string, int> _registers;
        private int _nextInstruction;
        private readonly string[] _instructionNames = new[] { "hlf", "tpl", "inc", "jmp", "jie", "jio" };
        private string[] _instructions;

        public Day23() : base(2015, 23)
        {
        }

        public string CalculatePartA()
        {
            Init(0);
            _instructions = GetInput();

            string instruction = GetInstruction();

            while (instruction != null)
            {
                ProcessInstruction(instruction);
                instruction = GetInstruction();
            }

            return _registers["b"].ToString();
        }

        public string CalculatePartB()
        {
            Init(1);
            _instructions = GetInput();

            string instruction = GetInstruction();
            _registers["a"] = 1;

            while (instruction != null)
            {
                ProcessInstruction(instruction);
                instruction = GetInstruction();
            }

            return _registers["b"].ToString();
        }

        private void Init(int aValue)
        {
            _nextInstruction = 0;

            _registers = new Dictionary<string, int>
            {
                { "a", aValue },
                { "b", 0 }
            };
        }

        private string GetInstruction()
        {
            if (_nextInstruction >= 0 && _nextInstruction < _instructions.Length)
                return _instructions[_nextInstruction];

            return null;
        }

        private void ProcessInstruction(string instruction)
        {            
            var instructionName = _instructionNames.Single(n => instruction.Contains(n));
            var register = _registers.SingleOrDefault(r => instruction.Contains($" {r.Key}"));
            var registerName = register.Key;
            int jump;

            switch (instructionName)
            {
                case "inc":                    
                    _registers[registerName]++;
                    _nextInstruction++;
                    break;
                case "jmp":
                    jump = int.Parse(instruction.Replace("jmp", string.Empty).Trim());
                    _nextInstruction += jump;
                    break;
                case "jio":
                    bool isOne = _registers[registerName] == 1;

                    jump = isOne 
                            ? int.Parse(instruction.Split(',')[1]) 
                            : 1;

                    _nextInstruction += jump;
                    break;
                case "jie":
                    bool isEven = _registers[registerName] % 2 == 0;

                    jump = isEven
                        ? int.Parse(instruction.Split(',')[1])
                        : 1;

                    _nextInstruction += jump;
                    break;
                case "hlf":
                    _registers[registerName] = _registers[registerName] / 2;
                    _nextInstruction++;
                    break;
                case "tpl":                    
                    _registers[registerName] = _registers[registerName] * 3;
                    _nextInstruction++;
                    break;                    
                default:
                    throw new Exception("instruction not implemented!");
            }
        }

    }
}
