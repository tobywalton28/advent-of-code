﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode.Days._2015
{
    public class Day10 : AdventDay, IAdventDay
    {
        public Day10() : base(2015, 10)
        {
        }

        public string CalculatePartA()
        {
            var input = "1321131112";

            foreach (var i in Enumerable.Range(1, 40))
            {
                IEnumerable<string> split = SplitByCharacter(input);
                input = Parse(split);
            }

            return input.Length.ToString();
        }

        public string CalculatePartB()
        {
            var input = "1321131112";

            foreach (var i in Enumerable.Range(1, 50))
            {
                IEnumerable<string> split = SplitByCharacter(input);
                input = Parse(split);
            }

            return input.Length.ToString();
        }

        private string Parse(IEnumerable<string> strings)
        {
            StringBuilder result = new StringBuilder();

            foreach (string str in strings)
            {
                string length = str.Length.ToString();
                string val = str.First().ToString();
                result.Append(length + val);
            }

            return result.ToString();
        }

        private IEnumerable<string> SplitByCharacter(string input)
        {

            var result = new List<string>();
            string working = string.Empty;

            foreach (char ch in input)
            {
                if (working == string.Empty || working.ToCharArray().Last() == ch)
                {
                    working = working + ch;
                }
                else
                {
                    result.Add(working);
                    working = ch.ToString();
                }
            }

            if (working != string.Empty) result.Add(working);

            return result;
        }
    }
}
