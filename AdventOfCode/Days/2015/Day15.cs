﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day15 : AdventDay, IAdventDay
    {
        public Day15() : base(2015, 15)
        {
        }

        public string CalculatePartA()
        {          
            var ingredients = GetIngredients();
            var split = Get100();
            var scores = split.Select(x => CalculateScore(x, ingredients)).ToList();

            return scores.Max(s => s.Score).ToString();
        }

        public string CalculatePartB()
        {
            var ingredients = GetIngredients();
            var split = Get100();
            var scores = split.Select(x => CalculateScore(x, ingredients)).ToList();

            var calories500 = scores.Where(s => s.Calories == 500).ToList();

            return calories500.OrderByDescending(c => c.Score).First().Score.ToString();
        }

        private List<int[]> Get100()
        {
            List<int[]> to100 = new List<int[]>();

            for (int pot1 = 0; pot1 <= 100; pot1++)
            {
                for (int pot2 = 0; pot2 <= 100; pot2++)
                {
                    for (int pot3 = 0; pot3 <= 100; pot3++)
                    {
                        for (int pot4 = 0; pot4 <= 100; pot4++)
                        {
                            int sum = pot1 + pot2 + pot3 + pot4;
                            if (sum == 100) to100.Add(new int[] { pot1, pot2, pot3, pot4 });
                        }
                    }
                }
            }

            return to100;
        }

        private Ingriedient[] GetIngredients()
        {
            var input = GetInput();

            return input.Select(str => {
                string name = str.Substring(0, str.IndexOf(':'));
                var numbers = Regex.Matches(str, $"-?\\d+").Cast<Match>().Select(m => int.Parse(m.Value)).ToArray();

                return new Ingriedient(name, numbers[0], numbers[1], numbers[2], numbers[3], numbers[4]);
            }).ToArray();
        }

        private Cookie CalculateScore(int[] spoons, Ingriedient[] ingriedients)
        {
            int capacity = 0;
            int durability = 0;
            int flavour = 0;
            int texture = 0;
            int calories = 0;

            for (int i = 0; i < spoons.Length; i++)
            {
                int spoonfulls = spoons[i];
                Ingriedient ingriedient = ingriedients[i];

                capacity += spoonfulls * ingriedient.Capacity;
                durability += spoonfulls * ingriedient.Durability;
                flavour += spoonfulls * ingriedient.Flavour;
                texture += spoonfulls * ingriedient.Texture;
                calories += spoonfulls * ingriedient.Calories;
            }

            int score = Math.Max(capacity, 0) * Math.Max(durability, 0) * Math.Max(flavour, 0) * Math.Max(texture, 0);

            return new Cookie(score, calories);
        }

        public class Cookie
        {
            public Cookie(int score, int calories)
            {
                Score = score;
                Calories = calories;
            }

            public int Score { get; set; }

            public int Calories { get; set; }
        }

        public class Ingriedient
        {
            public Ingriedient(string name, int capacity, int durability, int flavour, int texture, int calories)
            {
                Name = name;
                Capacity = capacity;
                Durability = durability;
                Flavour = flavour;
                Texture = texture;
                Calories = calories;
            }

            public string Name { get; set; }

            public int Capacity { get; set; }

            public int Durability { get; set; }

            public int Flavour { get; set; }

            public int Texture { get; set; }

            public int Calories { get; set; }
        }
    }
}
