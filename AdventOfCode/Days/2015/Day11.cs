﻿using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day11 : AdventDay, IAdventDay
    {
        private const int A_VALUE = 97;
        private const int Z_VALUE = 122;

        public Day11() : base(2015, 11)
        {
        }

        public string CalculatePartA()
        {
            string puzzleInput = "hxbxwxba";
            string newPassword = puzzleInput;
            bool passwordValid = false;
            int count = 0;

            while (!passwordValid)
            {
                newPassword = IncrementPassword(newPassword);
                passwordValid = ValidateIllegalCharacters(newPassword) && ValidateIncreasingStraight(newPassword) && ValidateLetterPairs(newPassword);
                count++;
            }

            return newPassword;
        }

        public string CalculatePartB()
        {
            string puzzleInput = "hxbxxyzz";
            string newPassword = puzzleInput;
            bool passwordValid = false;
            int count = 0;

            while (!passwordValid)
            {
                newPassword = IncrementPassword(newPassword);
                passwordValid = ValidateIllegalCharacters(newPassword) && ValidateIncreasingStraight(newPassword) && ValidateLetterPairs(newPassword);
                count++;
            }

            return newPassword;
        }

        private string IncrementPassword(string password)
        {
            var chars = password.ToCharArray().ToList();

            var charToIncrement = chars.Last(ch => (int) ch < Z_VALUE);

            var index = chars.LastIndexOf(charToIncrement);

            chars[index] = (char)((int)charToIncrement + 1);

            for (int i = index + 1; i < chars.Count; i++)
            {
                chars[i] = (char)A_VALUE;
            }

            return new string(chars.ToArray());            
        }

        private bool ValidateIncreasingStraight(string password)
        {
            var groupsOfThree = Enumerable.Range(0, password.Length - 2).Select(i => password.Substring(i, 3));

            return groupsOfThree.Any(grp => (int)grp[1] == (int)grp[0] + 1 && (int)grp[2] == (int)grp[0] + 2);
        }

        private bool ValidateIllegalCharacters(string password)
        {
            char[] illegalChars = { 'i', 'o', 'l' };

            return !password.ToCharArray().Any(ch => illegalChars.Contains(ch));
        }

        private bool ValidateLetterPairs(string password)
        {
            var groupsOfTwo = Enumerable.Range(0, password.Length - 1).Select(i => password.Substring(i, 2)).Where(str => str[0] == str[1]);
            var distinctGroups = groupsOfTwo.Select(grp => grp[0]).Distinct().Count();

            return distinctGroups >= 2;
        }
    }
}
