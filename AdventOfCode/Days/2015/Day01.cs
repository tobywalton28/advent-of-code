﻿namespace AdventOfCode.Days._2015
{
    public class Day01 : AdventDay, IAdventDay
    {
        public Day01() : base(2015, 1)
        {
        }

        public string CalculatePartA()
        {
            int result = 0;

            char[] input = GetInput()[0].ToCharArray();

            foreach (char c in input)
            {
                if (c == '(') result += 1;
                if (c == ')') result -= 1;
            }

            return result.ToString();
        }

        public string CalculatePartB()
        {
            int floor = 0;
            int position = 1;

            char[] input = GetInput()[0].ToCharArray();

            foreach (char c in input)
            {
                if (c == '(') floor += 1;
                if (c == ')') floor -= 1;
                if (floor < 0) return position.ToString();

                position++;
            }

            return "-1";
        }
    }
}
