﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day05 : AdventDay, IAdventDay
    {
        public Day05() : base(2015, 5)
        {
        }

        private char[] _vowels = { 'a', 'e', 'i', 'o', 'u' };
        private string[] _illegalStrings = { "ab", "cd", "pq", "xy" };

        public string CalculatePartA()
        {
            var input = GetInput();

            return input.Count(IsNice).ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput();

            return input.Count(IsNice2).ToString();
        }

        private bool IsNice(string input)
        {
            IEnumerable<CharacterPair> characterPairs = GetCharacterPairs(input);

            return input.ToCharArray().Count(c => _vowels.Contains(c)) >= 3
                   &&
                   characterPairs.Any(cp => cp.Value[0] == cp.Value[1])
                   &&
                   !_illegalStrings.Any(input.Contains);
        }

        private bool IsNice2(string input)
        {
            IEnumerable<CharacterPair> twos = GetCharacterPairs(input);
            IEnumerable<CharacterPair> threes = GetCharacterThrees(input);

            bool repeatingPair = twos.Any(thisTwo =>
            {
                CharacterPair[] matches = twos.Where(two => two.Value == thisTwo.Value).ToArray();
                int[] indexes = matches.Select(m => m.Index).ToArray();
                return matches.Count() >= 2 && indexes.All(i => i != thisTwo.Index + 1 && i != thisTwo.Index - 1);
            });

            return repeatingPair
                   &&
                   threes.Any(pair => pair.Value.First() == pair.Value.Last());
        }

        private List<CharacterPair> GetCharacterPairs(string input)
        {
            return Enumerable.Range(0, input.Length - 1)
                             .Select(i => new CharacterPair(input.Substring(i, 2), i))
                             .ToList();
        }

        private List<CharacterPair> GetCharacterThrees(string input)
        {
            return Enumerable.Range(0, input.Length - 2)
                             .Select(i => new CharacterPair(input.Substring(i, 3), i))
                             .ToList();
        }

        private class CharacterPair
        {
            public CharacterPair(string value, int index)
            {
                Value = value;
                Index = index;
            }

            public string Value { get; }

            public int Index { get; }
        }
    }
}
