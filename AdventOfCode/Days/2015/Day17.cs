﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day17 : AdventDay, IAdventDay
    {
        public Day17() : base(2015, 17)
        {
        }

        public string CalculatePartA()
        {
            var input = GetInput().Select(str => new { Capacity = int.Parse(str), ID = Guid.NewGuid() }).ToList();
            
            var combinations = Combos(input.ToArray());
            var combinations150 = combinations.Where(i => AddsTo150(i.Select(s => s.Capacity))).ToList();
            var keys = combinations150.Select(ints => string.Join(",", ints.OrderBy(i => i.ID)));
            
            return keys.Distinct().Count().ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput().Select(str => new { Capacity = int.Parse(str), ID = Guid.NewGuid() }).ToList();

            var combinations = Combos(input.ToArray());
            var combinations150 = combinations.Where(i => AddsTo150(i.Select(s => s.Capacity))).ToList();
            var minContainers = combinations150.Min(c => c.Count());
            var efficientCombinations = combinations150.Where(c => c.Count() == minContainers);
            var keys = efficientCombinations.Select(ints => string.Join(",", ints.OrderBy(i => i.ID)));
        
            return keys.Distinct().Count().ToString();
        }

        private bool AddsTo150(IEnumerable<int> numbers)
        {
            return numbers.Sum() == 150;
        }

        private IEnumerable<IEnumerable<T>> Combos<T>(T[] arr)
        {
            IEnumerable<T> DoQuery(long j, long idx)
            {
                do
                {
                    if ((j & 1) == 1) yield return arr[idx];
                } while ((j >>= 1) > 0 && ++idx < arr.Length);
            }
            for (var i = 0; i < Math.Pow(2, arr.Length); i++)
                yield return DoQuery(i, 0);
        }
    }
}
