﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day21 : AdventDay, IAdventDay
    {
        private Player _player;
        private Player _boss;

        public Day21() : base(2015, 21)
        {
        }

        public string CalculatePartA()
        {
            var itemCombinations = GetItemCombinations().ToList();

            var winningFights = itemCombinations.Select(items => new
            {
                Items = items,
                Cost = items.Sum(i => i.Cost),
                Result = Fight(items)
            }).Where(r => r.Result == "Player Wins");
            
            return winningFights
                        .OrderBy(r => r.Cost)
                        .First()
                        .Cost
                        .ToString();
        }

        public string CalculatePartB()
        {
            var itemCombinations = GetItemCombinations().ToList();

            var losingFights = itemCombinations.Select(items => new
            {
                Items = items,
                Cost = items.Sum(i => i.Cost),
                Result = Fight(items)
            }).Where(r => r.Result == "Boss Wins");

            return losingFights
                        .OrderByDescending(r => r.Cost)
                        .First()
                        .Cost
                        .ToString();
        }

        private string Fight(Item[] items)
        {
            var player = new Player(100, items.Sum(i => i.Damage), items.Sum(i => i.Armor));
            var boss = new Player(109, 8, 2);

            return Fight(player, boss);
        }

        private string Fight(Player player, Player boss)
        {
            int turn = 1;

            while (player.HitPoints > 0 && boss.HitPoints > 0)
            {
                var attacker = turn % 2 == 1 ? player : boss;
                var defender = turn % 2 == 0 ? player : boss;

                int damage = Math.Max(1, attacker.Damage - defender.Armor);
                defender.HitPoints -= damage;
                turn += 1;
            }

            return player.HitPoints > 0 ? "Player Wins" : "Boss Wins";
        }

        private Item[] GetItems()
        {
            return new Item[]
            {
                new Item(ItemType.Weapon, "Dagger", 8, 4, 0),
                new Item(ItemType.Weapon, "Shortsword", 10, 5, 0),
                new Item(ItemType.Weapon, "Warhammer", 25, 6, 0),
                new Item(ItemType.Weapon, "Longsword", 40, 7, 0),
                new Item(ItemType.Weapon, "Greataxe", 74, 8, 0),

                new Item(ItemType.Armor, "No Armour", 0, 0, 0),
                new Item(ItemType.Armor, "Leather", 13, 0, 1),
                new Item(ItemType.Armor, "Chainmail", 31, 0, 2),
                new Item(ItemType.Armor, "Splintmail", 53, 0, 3),
                new Item(ItemType.Armor, "Bandedmail", 75, 0, 4),
                new Item(ItemType.Armor, "Platemail", 102, 0, 5),

                new Item(ItemType.Ring, "Damage +1", 25, 1, 0),
                new Item(ItemType.Ring, "Damage +2", 50, 2, 0),
                new Item(ItemType.Ring, "Damage +3", 100, 3, 0),
                new Item(ItemType.Ring, "Defense +1", 20, 0, 1),
                new Item(ItemType.Ring, "Defense +2", 40, 0, 2),
                new Item(ItemType.Ring, "Defense +3", 80, 0, 3)
            };
        }

        private IEnumerable<IEnumerable<Item>> GetRingCombinations()
        {
            var rings = GetItems().Where(item => item.ItemType == ItemType.Ring);

            var ringPairs = Permutate.Permute(rings, 2);
            var singleRings = rings.Select(ring => new List<Item> { ring }.AsEnumerable());
            var noRings = new List<List<Item>> { new List<Item>() };

            return noRings.Concat(singleRings).Concat(ringPairs);
        }

        private IEnumerable<Item[]> GetItemCombinations()
        {
            var weapons = GetItems().Where(item => item.ItemType == ItemType.Weapon);
            var armours = GetItems().Where(item => item.ItemType == ItemType.Armor);
            
            foreach (var weapon in weapons)
            foreach(var armour in armours)
            foreach(var rings in GetRingCombinations())
            {
                yield return new Item[] { weapon, armour }.Concat(rings).ToArray();
            }
        }

        public enum ItemType
        {
            Weapon,
            Armor,
            Ring
        }

        public class Player
        {
            public int HitPoints { get; set; }

            public int Damage { get; private set; }

            public int Armor { get; private set; }

            public Player(int hitPoints, int damage, int armor)
            {
                HitPoints = hitPoints;
                Damage = damage;
                Armor = armor;
            }
        }

        public class Item
        {
            public ItemType ItemType { get; private set; }

            public string Name { get; private set; }

            public int Cost { get; private set; }

            public int Damage { get; private set; }

            public int Armor { get; private set; }

            public Item(ItemType itemType, string name, int cost, int damage, int armor)
            {
                ItemType = itemType;
                Name = name;
                Cost = cost;
                Damage = damage;
                Armor = armor;
            }
        }
    }
}
