﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day12 : AdventDay, IAdventDay
    {
        private int _result = 0;

        public Day12() : base(2015, 12)
        {
        }                

        public string CalculatePartA()
        {
            var input = GetInput()[0];            
            string numberPattern = $"-?\\d+";
            var matches = Regex.Matches(input, numberPattern);
            var numbers = matches.Cast<Match>().Select(m => int.Parse(m.Value));

            return numbers.Sum().ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput()[0];            
            var data = JObject.Parse(input);
            Traverse(data);
            return _result.ToString();
        }

        private void Traverse(JToken data)
        {
            foreach (JToken child in data.Children())
            {
                if (child.Type == JTokenType.Object)
                {
                    var obj = child as JObject;
                    if (!obj.Properties().Any(p => p.Value.ToString() == "red"))
                    {
                        Traverse(child);
                    }                    
                }
                else
                {
                    Traverse(child);
                }
                                
                if (child.Type == JTokenType.Integer) _result += child.Value<Int32>();
            }
        }
    }
}
