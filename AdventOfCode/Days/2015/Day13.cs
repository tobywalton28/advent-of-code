﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2015
{
    public class Day13 : AdventDay, IAdventDay
    {
        public Day13() : base(2015, 13)
        {
        }

        public string CalculatePartA()
        {
            var instructions = GetInstructions();
            var people = instructions.Select(i => i.Cause).Distinct().ToList();
            var permutations = Permutate.Permute(people, people.Count).ToList();
            var allScores = permutations.Select(p => ScorePermutation(p.ToArray(), instructions)).ToList();

            return allScores.Max().ToString();
        }

        public string CalculatePartB()
        {
            var instructions = GetInstructions();

            var guests = instructions.Select(i => i.Cause).Distinct().ToList();
            const string me = "Toby";

            foreach (var guest in guests)
            {
                instructions.Add(new Instruction(me, guest, 0));
                instructions.Add(new Instruction(guest, me, 0));
            }
            
            var people = instructions.Select(i => i.Cause).Distinct().ToList();
            var permutations = Permutate.Permute(people, people.Count).ToList();
            var allScores = permutations.Select(p => ScorePermutation(p.ToArray(), instructions)).ToList();

            return allScores.Max().ToString();
        }

        private List<Instruction> GetInstructions()
        {
            return GetInput().Select(str => {
                string recipient = str.Substring(0, str.IndexOf(' ')).Trim();
                string cause = str.Substring(str.LastIndexOf(' ')).Replace('.', ' ').Trim();
                int points = int.Parse(Regex.Match(str, $"\\d+").Value);
                if (str.Contains("lose")) points = -points;

                return new Instruction(recipient, cause, points);
            }).ToList();
        }

        private int ScorePermutation(string[] permutation, IEnumerable<Instruction> instructions)
        {
            int result = 0;

            for (int i = 0; i < permutation.Count(); i++)
            {
                string recipient = permutation[i];
                int indexA = i == 0 ? permutation.Length - 1 : i - 1;
                int indexB = i == permutation.Length - 1 ? 0 : i + 1;
                string neighbourA = permutation[indexA];
                string neighbourB = permutation[indexB];

                var instructionA = instructions.Single(ins => ins.Recipient == recipient && ins.Cause == neighbourA);
                var instructionB = instructions.Single(ins => ins.Recipient == recipient && ins.Cause == neighbourB);

                result += instructionA.Points;
                result += instructionB.Points;
            }

            return result;
        }

        private class Instruction
        {
            public Instruction(string recipient, string cause, int points)
            {
                Recipient = recipient;
                Cause = cause;
                Points = points;
            }

            public string Recipient { get; }

            public string Cause { get; }

            public int Points { get; set; }
        }
    }
}
