﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2015
{
    public class Day09 : AdventDay, IAdventDay
    {
        private Route[] _routes;

        public Day09() : base(2015, 9)
        {
        }

        public string CalculatePartA()
        {
            _routes = GetRoutes();

            var places = GetPlaces();

            var permutations = GetPermutations(places);

            var permutationDistances = permutations.Select(CalculateJourneyDistance).ToList();

            return permutationDistances.Min().ToString();
        }

        public string CalculatePartB()
        {
            _routes = GetRoutes();

            var places = GetPlaces();

            var permutations = GetPermutations(places);

            var permutationDistances = permutations.Select(CalculateJourneyDistance).ToList();

            return permutationDistances.Max().ToString();
        }

        private Route[] GetRoutes()
        {
            string[] input = GetInput();

            return input.Select(str => str.Replace(" to ", " ").Replace(" = ", " ").Split(' '))
                        .Select(arr => new Route(arr[0], arr[1], int.Parse(arr[2])))
                        .ToArray();
        }

        private int CalculateJourneyDistance(string[] route)
        {
            return Enumerable.Range(0, route.Length - 1)
                             .Select(i => GetDistance(route[i], route[i + 1]))
                             .Sum();
        }

        private string[] GetPlaces()
        {
            return _routes.Select(r => r.Start).Union(_routes.Select(r2 => r2.Finish))
                          .Distinct()
                          .ToArray();
        }

        private int GetDistance(string placeA, string placeB)
        {
            Route route = _routes.Single(r =>
                r.Start == placeA && r.Finish == placeB
                ||
                r.Start == placeB && r.Finish == placeA);

            return route.Distance;
        }

        private IEnumerable<T[]> GetPermutations<T>(IEnumerable<T> list)
        {
            List<T> items = list.ToList();

            return GetPermutations(items, items.Count);
        }

        private IEnumerable<T[]> GetPermutations<T>(List<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }).ToArray());
        }


        private class Route
        {
            public Route(string start, string finish, int distance)
            {
                Start = start;
                Finish = finish;
                Distance = distance;
            }

            public string Start { get; }

            public string Finish { get; }

            public int Distance { get; }
        }
    }
}
