﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2018
{
    public class Day01 : AdventDay, IAdventDay
    {
        public Day01() : base(2018, 1)
        {            
        }

        public string CalculatePartA()
        {
            int result = 0;

            foreach (var instruction in GetInstructions())
            {
                if (instruction.Operation == "+") result += instruction.Value;
                if (instruction.Operation == "-") result -= instruction.Value;
            }
    
            return result.ToString();
        }

        public string CalculatePartB()
        {
            int result = 0;
            Dictionary<int, int> history = new Dictionary<int, int>();
            history.Add(0, 1);

            while (true)
            {
                foreach (var instruction in GetInstructions())
                {
                    if (instruction.Operation == "+") result += instruction.Value;
                    if (instruction.Operation == "-") result -= instruction.Value;

                    if (history.ContainsKey(result))
                    {
                        history[result] += 1;
                    }
                    else
                    {
                        history.Add(result, 1);
                    }

                    if (history[result] == 2) return result.ToString();
                }
            }

        }

        private Instruction[] GetInstructions()
        {
            return GetInput()
                        .Select(i => new Instruction(i.Substring(0, 1), int.Parse(i.Substring(1))))
                        .ToArray();
        }

        private class Instruction
        {
            public Instruction(string operation, int value)
            {
                Operation = operation;
                Value = value;
            }

            public string Operation { get; }

            public int Value { get; }
        }
    }
}
