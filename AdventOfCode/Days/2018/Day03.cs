﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Days._2018
{
    public class Day03 : AdventDay, IAdventDay
    {
        Dictionary<int, int> counter = new Dictionary<int, int>();

        public Day03() : base(2018, 3)
        {            
        }

        public string CalculatePartA()
        {                        
            Claim[] claims = GetClaims();
         
            foreach (var claim in claims)
            {
                var claimLocations = GetClaimLocations(claim);
                foreach (var claimLocation in claimLocations)
                {
                    int key = GetLocationKey(claimLocation);
                    if (!counter.ContainsKey(key)) counter.Add(key, 0);
                    counter[key] += 1;                    
                }
            }

            return counter.Values.Count(v => v > 1).ToString();
        }

        public string CalculatePartB()
        {
            Claim[] claims = GetClaims();

            foreach (var claim in claims)
            {
                var locationKeys = GetClaimLocations(claim).Select(GetLocationKey);
                var overlapDetected = locationKeys.Any(k => counter[k] > 1);

                if (!overlapDetected) return claim.Id.ToString();
            }

            return "could not locate claim";
        }     

        private int GetLocationKey(Location location)
        {
            return (location.TopPosition * 10000) + location.LeftPosition;
        }   

        private Location[] GetClaimLocations(Claim claim)
        {
            List<Location> locations = new List<Location>();

            for (int top = claim.TopPosition; top < claim.TopPosition + claim.Height; top++)
            {
                for (int left = claim.LeftPosition; left < claim.LeftPosition + claim.Width; left++)
                {
                    locations.Add(new Location(left, top));
                }
            }

            return locations.ToArray();
        }

        private Claim[] GetClaims()
        {
            return GetInput().Select(i =>
            {
                string[] IdPositionAndDimensions = i.Split('@');
                int id = int.Parse(IdPositionAndDimensions[0].Replace("#", string.Empty).Trim());
                string[] positionAndDimensions = IdPositionAndDimensions[1].Split(':');
                int left = int.Parse(positionAndDimensions[0].Split(',')[0].Trim());
                int top = int.Parse(positionAndDimensions[0].Split(',')[1].Trim());
                int width = int.Parse(positionAndDimensions[1].Split('x')[0].Trim());
                int height = int.Parse(positionAndDimensions[1].Split('x')[1].Trim());

                return new Claim(id, left, top, width, height);
            }).ToArray();
        }

        private class Claim
        {
            public Claim(int id, int leftPosition, int topPosition, int width, int height)
            {
                Id = id;
                LeftPosition = leftPosition;
                TopPosition = topPosition;
                Width = width;
                Height = height;
            }

            public int Id { get; private set; }

            public int LeftPosition { get; private set; }

            public int TopPosition { get; private set; }

            public int Width { get; private set; }

            public int Height { get; private set; }
        }

        private class Location
        {
            public Location(int leftPosition, int topPosition)
            {
                LeftPosition = leftPosition;
                TopPosition = topPosition;
            }

            public int LeftPosition { get; }

            public int TopPosition { get; }
        }
    }
}
