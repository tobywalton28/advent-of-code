﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode.Days._2018
{
    public class Day02 : AdventDay, IAdventDay
    {
        public Day02() : base(2018, 2)
        {            
        }

        public string CalculatePartA()
        {
            List<Dictionary<char, int>> boxData = GetInput().Select(ParseBoxId).ToList();

            int boxesWithTwoOfAnyLetter = boxData.Count(bd => bd.Any(kvp => kvp.Value == 2));
            int boxesWithThreeOfAnyLetter = boxData.Count(bd => bd.Any(kvp => kvp.Value == 3));

            return (boxesWithTwoOfAnyLetter * boxesWithThreeOfAnyLetter).ToString();
        }

        public string CalculatePartB()
        {
            var input = GetInput();

            var stringPairs = input.SelectMany(i => input.Select(inner => new { A = i, B = inner, Diff = CalculateDifference(i, inner) }));

            var pair = stringPairs.First(sp => sp.Diff == 1);

            return RemoveDifference(pair.A, pair.B);
        }

        private string RemoveDifference(string boxA, string boxB)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < boxB.Length; i++)
            {
                if (boxA[i] == boxB[i]) sb.Append(boxA[i]);
            }

            return sb.ToString();
        }

        private int CalculateDifference(string boxA, string boxB)
        {
            var charsBoxA = boxA.ToCharArray();
            var charsBoxB = boxB.ToCharArray();

            int difference = 0;
            for (int idx = 0; idx < charsBoxA.Length; idx++)
            {
                if (charsBoxA[idx] != charsBoxB[idx]) difference++;
            }

            return difference;
        }

        private Dictionary<char, int> ParseBoxId(string boxId)
        {
            return boxId.ToCharArray()
                        .GroupBy(ch => ch)
                        .ToDictionary(grp => grp.Key, grp => grp.Count());            
        }
    }
}
