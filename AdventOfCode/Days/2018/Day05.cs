﻿using System.Linq;
using System.Text;

namespace AdventOfCode.Days._2018
{
    public class Day05 : AdventDay, IAdventDay
    {
        public Day05() : base(2018, 5)
        {
        }

        public string CalculatePartA()
        {            
            string input = GetInput()[0];
            var result = CalculatePolymer(input);

            return result.Length.ToString();
        }

        public string CalculatePartB()
        {
            string input = GetInput()[0];
            var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var alphabetPolymers = alphabet.Select(letter => CalculatePolymer(RemoveCharacter(input, letter)).Length).ToList();
            var shortestPolymer = alphabetPolymers.Min();

            return shortestPolymer.ToString();
        }

        private string RemoveCharacter(string input, char characterToRemove)
        {
            return input.Replace(characterToRemove.ToString(), string.Empty)
                .Replace(characterToRemove.ToString().ToLower(), string.Empty);
        }

        private string CalculatePolymer(string input)
        {
            StringBuilder result = new StringBuilder();

            foreach (var ch in input)
            {
                result.Append(ch);

                if (result.Length >= 2)
                {
                    var last = result[result.Length - 1];
                    var penultimate = result[result.Length - 2];

                    if (last != penultimate && last.ToString().ToUpper() == penultimate.ToString().ToUpper())
                    {
                        result.Length = result.Length - 2;
                    }
                }
            }

            return result.ToString();
        }
    }
}
