﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Days._2018
{
    public class Day04 : AdventDay, IAdventDay
    {        
        public Day04() : base(2018, 4)
        {            
        }

        public string CalculatePartA()
        {
            var instructions = GetInstructions();
            var processed = ProcessInstructions(instructions);
                        
            var totalSpeep = processed.Select(p => new { GuardNumber = p.Key, TotalMinutesAsleep = TotalSleep(p.Value) });
            var sleepyGuard = totalSpeep.OrderByDescending(s => s.TotalMinutesAsleep).First();
            var minutes = GetSleepMinutes(processed[sleepyGuard.GuardNumber]);
            var sleepyMinute = minutes.OrderByDescending(m => m.Value).First().Key;

            return (sleepyGuard.GuardNumber * sleepyMinute).ToString();
        }     

        public string CalculatePartB()
        {
            var instructions = GetInstructions();
            var processed = ProcessInstructions(instructions);

            var totalSpeep = processed.SelectMany(p => GetSleepMinutes(p.Value).Select(m => new {
                Minute = m.Key,
                Sleep = m.Value,
                GuardNumber = p.Key
            })).ToList();

            var sleepiestMinute = totalSpeep.OrderByDescending(s => s.Sleep).First();

            return (sleepiestMinute.GuardNumber * sleepiestMinute.Minute).ToString();
        }

        private Dictionary<int, int> GetSleepMinutes(List<SleepPeriod> periods)
        {
            var result = Enumerable.Range(0, 60).ToDictionary(i => i, i => 0);

            foreach (SleepPeriod period in periods)
            {
                var asleepMin = period.FallAsleep.TimeOfDay.Minutes;
                var awakeMin = period.WakeUp.TimeOfDay.Minutes;

                for (int min = asleepMin; min < awakeMin; min++) result[min] += 1;
            }

            return result;
        }

        private TimeSpan TotalSleep(List<SleepPeriod> periods)
        {
            return periods.Aggregate(TimeSpan.Zero, (seed, func) => seed + (func.WakeUp - func.FallAsleep));
        }

        private Instruction[] GetInstructions()
        {            
            return GetInput().Select(MapInstruction)
                             .OrderBy(i => i.Date)
                             .ToArray();            
        }

        private Dictionary<int, List<SleepPeriod>> ProcessInstructions(Instruction[] instructions)
        {
            var result = new Dictionary<int, List<SleepPeriod>>();

        int guardNumber = 0;
        DateTime wakeUp = DateTime.MinValue;
        DateTime fallAsleep = DateTime.MinValue;

            foreach (Instruction i in instructions)
            {
                if (i.Action == GuardAction.BeginShift) guardNumber = i.GuardNumber;
                if (i.Action == GuardAction.FallAsleep) fallAsleep = i.Date;
                if (i.Action == GuardAction.WakeUp)
                {
                    wakeUp = i.Date;
                    if (!result.ContainsKey(guardNumber)) result.Add(guardNumber, new List<SleepPeriod>());
                    result[guardNumber].Add(new SleepPeriod(fallAsleep, wakeUp));
                }
}

            return result;
        }

        private Instruction MapInstruction(string input)
        {            
            DateTime date = DateTime.Parse(input.Substring(1, input.IndexOf(']') - 1));
            int guardNumber = 0;
            string guardNumberPattern = @"#\d*";
            var match = Regex.Match(input, guardNumberPattern);
            if (match.Success) guardNumber = int.Parse(match.Value.Replace("#", string.Empty));


            return new Instruction(date, guardNumber, GetAction(input));
        }

        private GuardAction GetAction(string input)
        {
            if (input.Contains("begins shift")) return GuardAction.BeginShift;
            if (input.Contains("wakes up")) return GuardAction.WakeUp;
            if (input.Contains("falls asleep")) return GuardAction.FallAsleep;

            throw new Exception("unknown guard action!");
        }

        public class Instruction
        {
            public Instruction(DateTime date, int guardNumber, GuardAction action)
            {
                Date = date;
                GuardNumber = guardNumber;
                Action = action;
            }

            public DateTime Date { get; }

            public int GuardNumber { get; }

            public GuardAction Action { get; }
        }

        public class SleepPeriod
        {
            public SleepPeriod(DateTime fallAsleep, DateTime wakeUp)
            {
                FallAsleep = fallAsleep;
                WakeUp = wakeUp;
            }

            public DateTime FallAsleep { get; }

            public DateTime WakeUp { get; }
        }

        public enum GuardAction
        {
            BeginShift,
            FallAsleep,
            WakeUp
        }
    }
}
