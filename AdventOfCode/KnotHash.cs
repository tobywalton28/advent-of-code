﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    class KnotHash
    {
        private int[] _list;
        private int[] _lengths;
        private int _currentPosition = 0;
        private int _skipSize = 0;

        public string Calculate(string input)
        {
            _currentPosition = 0;
            _skipSize = 0;

            _list = GenerateInitialList();
            _lengths = GetLengths(input);

            for (int round = 1; round <= 64; round++)
            {
                foreach (var length in _lengths)
                {
                    ProcessLength(length);
                }
            }

            var denseHash = CalculateDenseHash(_list);
            var knotHash = CalculateKnotHash(denseHash);

            return knotHash;
        }

        private int[] GenerateInitialList()
        {
            return Enumerable.Range(0, 256).ToArray();
        }

        private int[] GetLengths(string input)
        {
            var lengths = input.ToCharArray().Select(ch => (int)ch).ToList();
            lengths.AddRange(new List<int> { 17, 31, 73, 47, 23 });

            return lengths.ToArray();
        }

        private void ProcessLength(int length)
        {
            if (length > 0)
            {

                if (_currentPosition + length <= _list.Length - 1)
                {
                    var start = _list.ToList().Take(_currentPosition).ToList();
                    var middle = _list.ToList().Skip(_currentPosition).Take(length).ToList();
                    middle.Reverse();
                    var end = _list.ToList().Skip(_currentPosition + length).ToList();

                    start.AddRange(middle);
                    start.AddRange(end);

                    _list = start.ToArray();
                }
                else
                {
                    var itemsToReOrder = _list.Skip(_currentPosition).ToList();
                    itemsToReOrder.AddRange(_list.Take(length - itemsToReOrder.Count()));
                    itemsToReOrder.Reverse();

                    int nonSortableCount = _list.Length - length;
                    int nonSortableStart = _currentPosition - nonSortableCount;
                    var staticItems = _list.Skip(nonSortableStart).Take(nonSortableCount).ToList();

                    var sectionOne = itemsToReOrder.Skip(_list.Count() - _currentPosition).ToList();
                    var sectionThree = itemsToReOrder.Take(_list.Count() - _currentPosition).ToList();

                    var newList = sectionOne;
                    newList.AddRange(staticItems);
                    newList.AddRange(sectionThree);

                    _list = newList.ToArray();
                }

            }

            _currentPosition += (length + _skipSize);

            while (_currentPosition > _list.Length - 1)
                _currentPosition -= _list.Length;

            _skipSize += 1;
        }

        private string CalculateKnotHash(int[] denseHash)
        {
            var hex = denseHash.Select(val => val.ToString("X2"));

            return string.Join(string.Empty, hex);
        }

        private int[] CalculateDenseHash(int[] values)
        {
            var result = new List<int>();

            for (int block = 1; block <= 16; block++)
            {
                var blockValues = values.Skip(16 * (block - 1)).Take(16).ToList();
                var blockAggregate = blockValues.Aggregate((a, b) => a ^ b);
                result.Add(blockAggregate);
            }

            return result.ToArray();
        }
    }
}
