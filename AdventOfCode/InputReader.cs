﻿namespace AdventOfCode
{
    public class InputReader
    {
        public string[] GetInput(int year, int day)
        {
            string path = System.IO.Path.GetFullPath($"Input/{year}/Day{day:00}.txt");

            return System.IO.File.ReadAllLines(path);
        }
    }
}
