﻿using System;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Year:");
            int year = int.Parse(Console.ReadLine());

            var allDays = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IAdventDay).IsAssignableFrom(p) && p.IsClass);


            var adventDays = allDays.Select(type => (IAdventDay)Activator.CreateInstance(type))
                                    .Where(ad => ad.Year == year && ad.Day == 23)
                                    .OrderBy(ad => ad.Day);

            foreach (var day in adventDays)
            {                                
                Console.WriteLine(Calculate(day.Day, 'A', day.CalculatePartA));
                Console.WriteLine(Calculate(day.Day, 'B', day.CalculatePartB));
                Console.WriteLine(string.Empty);
            }

            int completedDays = allDays.Count();
            int totalDays = allDays.Select(type => (IAdventDay)Activator.CreateInstance(type)).Select(d => d.Year).Distinct().Count() * 25;

            Console.WriteLine($"Completed {completedDays} of {totalDays} days");
            Console.WriteLine("PRESS ENTER TO EXIT");
            Console.ReadLine();
        }

        static string Calculate(int day, char part, Func<String> calculator)
        {
            var sw = new Stopwatch();
            sw.Start();
            string result = calculator();
            sw.Stop();

            return $"Day {day} Part {part}: {result} Seconds: {sw.Elapsed.TotalSeconds:0.00}";
        }
    }
}
