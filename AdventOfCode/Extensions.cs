﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public static class Extensions
    {
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> selector)
        {
            return items.SelectMany(c => selector(c).Flatten(selector)).Concat(items);
        }
    }
}
